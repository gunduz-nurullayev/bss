from django.conf.urls import url
from django.conf.urls import include

import Auth
from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<CompanyTypeSlug>[\w-]+)/$', views.CompanyTypePage, name='CompanyTypePage'),
    url(r'^(?P<CompanyTypeSlug>[\w-]+)/(?P<SupportSlug>[\w-]+)/$', views.SupportPage, name='SupportPage'),

    url(r'^(?P<CompanyTypeSlug>[\w-]+)/(?P<SupportSlug>[\w-]+)/login/', include('Auth.urls')),
	url(r'^(?P<CompanyTypeSlug>[\w-]+)/(?P<SupportSlug>[\w-]+)/order/$', views.OrderPage, name='OrderPage'),
	url(r'^(?P<CompanyTypeSlug>[\w-]+)/(?P<SupportSlug>[\w-]+)/order/process/$', views.OrderProcess, name='OrderProcess'),
    #url(r'^(?P<CompanyTypeSlug>[\w-]+)/(?P<SupportSlug>[\w-]+)/order/$', views.OrderPage, name='OrderPage'),

    #url(r'^(?P<Company_id>[0-9]+)/$', views.CompanyTypePage, name='CompanyTypePage'),
    # url(r'^(?P<Company_id>[0-9]+)/package-(?P<Package_id>[0-9]+)/$', views.PackageDetails, name='PackageDetails'),
]

