from django import forms

from .models import CompanyCategory

from .models import CompanyType

from .models import Support

from .models import SupportAdvantage

from .models import Service

from ckeditor.widgets import CKEditorWidget


class CompanyCategoryChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
         return obj.name

class CompanyTypeForm(forms.ModelForm):

    description = forms.CharField(widget=CKEditorWidget())
    CompanyCategory = CompanyCategoryChoiceField(queryset=CompanyCategory.objects.all())

    class Meta:
        model = CompanyType
        fields = '__all__'





class CompanyTypeChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
         return obj.name

class SupportForm(forms.ModelForm):

    CompanyType = CompanyTypeChoiceField(queryset=CompanyType.objects.all())

    class Meta:
        model = Support
        fields = '__all__'




class SupportChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
         return obj.name

class ServiceForm(forms.ModelForm):

    Support = SupportChoiceField(queryset=Support.objects.all())

    class Meta:
        model = Service
        fields = '__all__'




class SupportAdvantageForm(forms.ModelForm):

    Support = SupportChoiceField(queryset=Support.objects.all())

    class Meta:
        model = SupportAdvantage
        fields = '__all__'


class OrderForm(forms.Form):
    production = forms.CharField(widget=forms.Textarea)
    cocustomer = forms.CharField(widget=forms.Textarea)
    workregion = forms.CharField(widget=forms.Textarea)

    prev = forms.CharField()





