from django.contrib import admin

from .models import CompanyCategory,CompanyType,Support,SupportAdvantage,Service

from .forms import SupportForm
from .forms import SupportAdvantageForm
from .forms import ServiceForm
from .forms import CompanyTypeForm

@admin.register(CompanyCategory)
class CompanyCategoryAdmin(admin.ModelAdmin):
	list_display = ['name']


@admin.register(CompanyType)
class CompanyTypeAdmin(admin.ModelAdmin):
	list_display = ['name']
	form = CompanyTypeForm

@admin.register(Support)
class SupportAdmin(admin.ModelAdmin):
	list_display = ['name']
	form = SupportForm


@admin.register(SupportAdvantage)
class SupportAdvantageAdmin(admin.ModelAdmin):
	list_display = ['name']
	form = SupportAdvantageForm

@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
	list_display = ['name']
	form = ServiceForm

# Register your models here.
