from django.db import models

# Create your models here.

class CompanyCategory(models.Model):				#Example: Industry, Tourism, Furniture..
    name = models.CharField(max_length=200)
    number = models.IntegerField()
    image = models.ImageField('Image',upload_to='uploads/company/category/')
    slug = models.SlugField()
    pub_date = models.DateTimeField('date published')

class CompanyType(models.Model):						#Example: Project companies, Furniture producers..
    CompanyCategory = models.ForeignKey(CompanyCategory, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    presentation = models.FileField(upload_to='uploads/company/type/presentation/')
    description = models.TextField(max_length=100000)
    slug = models.SlugField()

class Support(models.Model):					#Example: Gold, Silver, Bronze, Standart
	CompanyType = models.ForeignKey(CompanyType, on_delete=models.CASCADE)

	name = models.CharField(max_length=200)
	description = models.TextField(max_length=100000)
	image = models.ImageField('Image',upload_to='uploads/support/image/')
	presentation = models.FileField(upload_to='uploads/support/presentation')
	price = models.IntegerField()
	bonuses = models.TextField(max_length=100000,default = '0')
	slug = models.SlugField()
	realizationDuration = models.CharField(max_length=2000)

	# def __unicode__(self):
 #        return self.name

class SupportAdvantage(models.Model):					#Example: Gold, Silver, Bronze, Standart, Europe
	Support = models.ForeignKey(Support, on_delete=models.CASCADE)

	name = models.CharField(max_length=200)
	description = models.TextField(max_length=100000)
	image = models.ImageField()
	slug = models.SlugField()
	lang = models.CharField(max_length=200)

class Service(models.Model):						#Example: Corporate portal, Corporate portal + e-commerce..
	Support = models.ForeignKey(Support, on_delete=models.CASCADE)
	name = models.CharField(max_length=200)
	description = models.TextField(max_length=100000)
	slug = models.SlugField()
	#Price (Some variants[columns] may be added to prices in future [about the PackageRegion])
	price = models.IntegerField()


# class Order(models.Model):
	



