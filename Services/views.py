from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect

from django.template import loader

from django.shortcuts import get_object_or_404, render


from core.models import Menu
from core.models import Slide
from Auth.models import User
from Auth.models import Order

from .models import CompanyCategory
from .models import CompanyType

from .models import Support

from .models import SupportAdvantage

from .models import Service

from .forms import OrderForm

# Create your views here.

def index(request):													#Company Categories and Types
	MenuList = Menu.objects.order_by('number')
	SlideList = Slide.objects.order_by('number')
	MyUser = 0
	if(request.session.get('user')):
		MyUser = request.session['user']

	CompanyCategoryList = CompanyCategory.objects.order_by('number')
	CompanyTypeList = CompanyType.objects.order_by('id')

	context = {
		'MyUser': MyUser,
		'MenuList': MenuList,
		'SlideList': SlideList,
		'CompanyCategoryList': CompanyCategoryList,
		'CompanyTypeList': CompanyTypeList,
	}
	template = loader.get_template('services/index.html')
	return HttpResponse(template.render(context, request))



def CompanyTypePage(request,CompanyTypeSlug):
	MenuList = Menu.objects.order_by('number')
	SlideList = Slide.objects.order_by('number')

	# MyUser = User.objects.latest('id')
	# request.session['user'] = MyUser.id
	MyUser = 0
	if(request.session.get('user')):
		MyUser = request.session['user']

	MyCompanyType = CompanyType.objects.get(slug=CompanyTypeSlug)
	SupportList = Support.objects.filter(CompanyType_id=MyCompanyType)
	ServiceList = Service.objects.order_by('id')
	#Service_line = Services.objects.order_by('id')
	context = {
		'MyUser': MyUser,
		'MenuList': MenuList,
		'SlideList': SlideList,
		'MyCompanyType': MyCompanyType,
		# 'MyCompany': MyCompany,
		'SupportList': SupportList,
		'ServiceList': ServiceList,
		#'Service_line': Service_line
	}

	template = loader.get_template('services/companytype.html')
	return HttpResponse(template.render(context, request))


def SupportPage(request,CompanyTypeSlug,SupportSlug):
	MenuList = Menu.objects.order_by('number')
	SlideList = Slide.objects.order_by('number')

	# MyUser = User.objects.latest('id')
	# request.session['user'] = MyUser.id
	MyUser = 0
	if(request.session.get('user')):
		MyUser = request.session['user']

	MyCompanyType = get_object_or_404(CompanyType,slug=CompanyTypeSlug)
	MySupport = get_object_or_404(Support,CompanyType_id=MyCompanyType,slug=SupportSlug)
	SupportAdvantageList = SupportAdvantage.objects.filter(Support_id=MySupport)
	#SupportList = Support.objects.filter(CompanyType_id=MyCompanyType)
	ServiceList = Service.objects.filter(Support_id=MySupport)
	#Service_line = Services.objects.order_by('id')
	context = {
		'MyUser':MyUser,
		'MenuList': MenuList,
		'SlideList': SlideList,
		'MySupport': MySupport,
		# 'MyCompany': MyCompany,
		#'SupportList': SupportList,
		'SupportAdvantageList':SupportAdvantageList,
		'ServiceList': ServiceList,
		#'Service_line': Service_line
	}

	template = loader.get_template('services/supportpage.html')
	return HttpResponse(template.render(context, request))


def OrderPage(request,CompanyTypeSlug,SupportSlug):
	MenuList = Menu.objects.order_by('number')
	SlideList = Slide.objects.order_by('number')

	# MyUser = User.objects.latest('id')
	# request.session['user'] = MyUser.id
	MyUser = 0
	if(request.session.get('user')):
		MyUser = request.session['user']

	MyCompanyType = get_object_or_404(CompanyType,slug=CompanyTypeSlug)
	MySupport = get_object_or_404(Support,CompanyType_id=MyCompanyType,slug=SupportSlug)
	SupportAdvantageList = SupportAdvantage.objects.filter(Support_id=MySupport)
	#SupportList = Support.objects.filter(CompanyType_id=MyCompanyType)
	ServiceList = Service.objects.filter(Support_id=MySupport)
	#Service_line = Services.objects.order_by('id')
	context = {
		'MyUser': MyUser,
		'MenuList': MenuList,
		'SlideList': SlideList,
		'MySupport': MySupport,
		# 'MyCompany': MyCompany,
		#'SupportList': SupportList,
		'SupportAdvantageList':SupportAdvantageList,
		'ServiceList': ServiceList,
		#'Service_line': Service_line
	}


	template = loader.get_template('services/orderpage.html')
	return HttpResponse(template.render(context, request))

def OrderProcess(request,CompanyTypeSlug,SupportSlug):
	# MyUser = User.objects.latest('id')
	# request.session['user'] = MyUser.id
	MyUser = 0
	if(request.session.get('user')):
		MyUser = request.session['user']

	MyCompanyType = get_object_or_404(CompanyType,slug=CompanyTypeSlug)
	MySupport = get_object_or_404(Support,CompanyType_id=MyCompanyType,slug=SupportSlug)

	if request.method == 'POST':
		# create a form instance and populate it with data from the request:
		form = OrderForm(request.POST)
		# check whether it's valid:
		if form.is_valid():
			production = form.cleaned_data['production']
			WorkRegions = form.cleaned_data['workregion']
			CompanyCustomers = form.cleaned_data['cocustomer']

			prev = form.cleaned_data['prev']

			NewOrder = Order(Name=MySupport.name,User_id=MyUser,Support=MySupport,CompanyProduction=production,CompanyCustomers=CompanyCustomers,WorkRegions=WorkRegions,price=0,slug=SupportSlug)
			NewOrder.save()

			return redirect('http://127.0.0.1:8000/cabinet/')


