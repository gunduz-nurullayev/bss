# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-06 07:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Services', '0003_auto_20170505_1508'),
    ]

    operations = [
        migrations.AlterField(
            model_name='supportadvantage',
            name='image',
            field=models.ImageField(upload_to=''),
        ),
    ]
