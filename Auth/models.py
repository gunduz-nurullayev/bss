from django.db import models
import Services
from Services.models import Support
from Services.models import Service

class User(models.Model):
	CompanyName = models.CharField(max_length=200)
	CompanyRegion = models.CharField(max_length=200)

	login = models.CharField(max_length=200)
	password = models.CharField(max_length=2000)

	PhoneNumber = models.BigIntegerField()
	email = models.EmailField()

class Order(models.Model):
	User = models.ForeignKey(User, on_delete=models.CASCADE)
	Support = models.ForeignKey(Support,on_delete=models.CASCADE)

	Name = models.CharField(max_length=200)
	CompanyProduction = models.TextField(max_length=10000)
	CompanyCustomers = models.TextField(max_length=1000)
	WorkRegions = models.TextField(max_length=10000)

	OrderLevel = models.IntegerField(default=1)
	slug = models.CharField(max_length=200)
	price = models.IntegerField()

class AdditionalServiceList(models.Model):
	Order = models.ForeignKey(Order, on_delete=models.CASCADE)
	Service = models.ForeignKey(Service, on_delete=models.CASCADE)

# class SaleRecommendation(models.Model):
# 	Order = models.ForeignKey(Order, on_delete=models.CASCADE)
# 	User = models.ForeignKey(User, on_delete=models.CASCADE)
# 	Service = models.ForeignKey(Service, on_delete=models.CASCADE)

# 	Justification = models.TextField(max_length=10001)

class ProjectRecommendation(models.Model):
	Order = models.ForeignKey(Order, on_delete=models.CASCADE)
	User = models.ForeignKey(User, on_delete=models.CASCADE)
	Service = models.ForeignKey(Service, on_delete=models.CASCADE)

	Author = models.IntegerField()
	Apply = models.BooleanField(default=0)
	read = models.BooleanField(default=0)
	
	Justification = models.TextField(max_length=10000)

# Create your models here.
