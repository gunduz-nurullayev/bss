from django import forms

from registration.forms import RegistrationForm

from Services.models import Service

from .models import User
from .models import Order
from .models import ProjectRecommendation

from ckeditor.widgets import CKEditorWidget



class LoginForm(forms.Form):
	login = forms.CharField(max_length=100)
	password = forms.CharField(max_length=100)
	prev = forms.CharField()

class RegForm(forms.Form):
	login = forms.CharField(max_length=100)
	coname = forms.CharField(max_length=100)
	coregion = forms.CharField(max_length=100)
	email = forms.EmailField(max_length=100)
	password = forms.CharField(max_length=100)
	passagain = forms.CharField(max_length=100)
	phone = forms.IntegerField()
	prev = forms.CharField()


class MyCustomUserForm(RegistrationForm):
	login = forms.CharField(label='Your Name', max_length=100)
	class Meta:	
		model = User
		fields = '__all__'





class UserChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
         return obj.login

class OrderChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
         return obj.Name

class ServiceChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
         return obj.name

class ProjectRecommendationForm(forms.ModelForm):
	Justification = forms.CharField(widget=CKEditorWidget())
	User = UserChoiceField(queryset=User.objects.all())
	Order = OrderChoiceField(queryset=Order.objects.all())
	Service = ServiceChoiceField(queryset=Service.objects.all())

	class Meta:
		model = ProjectRecommendation
		fields = '__all__'
