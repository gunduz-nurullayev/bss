from django.contrib import admin


from Services.models import Service


from django.shortcuts import get_object_or_404

from .models import Order

from .models import User

from .models import ProjectRecommendation

from .forms import ProjectRecommendationForm
# Register your models here.



@admin.register(ProjectRecommendation)
class ProjectRecommendationAdmin(admin.ModelAdmin):
	list_display = ['getUser','getOrder','getService']
	form = ProjectRecommendationForm

	def getUser(self,obj):
		MyUser = get_object_or_404(User,id=obj.User_id)
		return MyUser.login
	def getOrder(self,obj):
		MyOrder = get_object_or_404(Order,id=obj.Order_id)
		return MyOrder.Name
	def getService(self,obj):
		MyService = get_object_or_404(Service,id=obj.Service_id)
		return MyService.name
