# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-09 13:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Auth', '0005_auto_20170509_1640'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='Name',
            field=models.CharField(default=0, max_length=200),
            preserve_default=False,
        ),
    ]
