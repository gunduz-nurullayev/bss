import os
from django.core.urlresolvers import reverse


from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect

from django.template import loader

from django.shortcuts import get_object_or_404, render

import hashlib
from django.shortcuts import redirect

from core.models import Menu
from core.models import Slide

from .models import User

from .forms import LoginForm
from .forms import RegForm

def index(request):

	MenuList = Menu.objects.order_by('number')
	SlideList = Slide.objects.order_by('number')
	MyUser = 0
	if(request.session.get('user')):
		MyUser = request.session['user']

	context = {
		'MyUser': MyUser,
		'MenuList': MenuList,
		'SlideList': SlideList,
	}
	template = loader.get_template('Auth/index.html')
	return HttpResponse(template.render(context, request))

def indexAfter(request,CompanyTypeSlug,SupportSlug):

	MenuList = Menu.objects.order_by('number')
	SlideList = Slide.objects.order_by('number')

	# MyDomain = request.META['HTTP_HOST']
	# MyUrlArray = (MyDomain,'services',CompanyTypeSlug,SupportSlug)
	# MyUrl = '/'.join(MyUrlArray)
	# #return HttpResponse(MyUrl)
	# return HttpResponseRedirect(MyUrl)
	MyUser = 0
	if(request.session.get('user')):
		MyUser = request.session['user']

	context = {
		'MyUser': MyUser,
		'MenuList': MenuList,
		'SlideList': SlideList,
	}
	template = loader.get_template('Auth/index.html')
	return HttpResponse(template.render(context, request))

def LogOut(request):
	
	del request.session['user']
	Logged = 'You logged out'
	return redirect('http://127.0.0.1:8000/')


def LoginProcess(request):
	# if this is a POST request we need to process the form data
	if request.method == 'POST':
		# create a form instance and populate it with data from the request:
		form = LoginForm(request.POST)
		# check whether it's valid:
		if form.is_valid():
			login = form.cleaned_data['login']
			password = form.cleaned_data['password']

			if User.objects.filter(login=login,password=password).exists():
				MyUser = get_object_or_404(User,login=login,password=password)
				request.session['user'] = MyUser.id

	context = {
		'login':login
	}

	return redirect('http://127.0.0.1:8000/')

def LoginProcessAfterSupportSelect(request,CompanyTypeSlug,SupportSlug):
	# if this is a POST request we need to process the form data
	if request.method == 'POST':
		# create a form instance and populate it with data from the request:
		form = LoginForm(request.POST)
		# check whether it's valid:
		if form.is_valid():
			login = form.cleaned_data['login']
			password = form.cleaned_data['password']

			prev = form.cleaned_data['prev']
			# -------------------------------------
			password = password.encode('utf-8')
			#password = hashlib.md5(password)

			if User.objects.filter(login=login,password=password).exists():
				MyUser = get_object_or_404(User,login=login,password=password)
				request.session['user'] = MyUser.id

				return redirect('http://127.0.0.1:8000/services/'+CompanyTypeSlug+'/'+SupportSlug)
			else:
				Alert = 'You have entered invalid username or password'
				return HttpResponse(Alert)

		else:
			NotValdForm = "Not valid Form"
			return HttpResponse(NotValdForm)


def RegProcess(request):
	# if this is a POST request we need to process the form data
	if request.method == 'POST':
		# create a form instance and populate it with data from the request:
		form = RegForm(request.POST)
		# check whether it's valid:
		if form.is_valid():
			login = form.cleaned_data['login']
			CompanyName = form.cleaned_data['coname']
			CompanyRegion = form.cleaned_data['coregion']
			password = form.cleaned_data['password']
			passagain = form.cleaned_data['passagain']
			phone = form.cleaned_data['phone']
			mail = form.cleaned_data['email']
			if(passagain == password):
				if User.objects.filter(login=login).exists():
					raise forms.ValidationError(u'Username "%s" is already in use.' % username)
				else:
					password = password.encode('utf-8')
					#password = hashlib.sha256(password)
					NewUser = User(login=login,CompanyName=CompanyName,CompanyRegion=CompanyRegion,password=password,PhoneNumber=phone,email=mail)
					NewUser.save()

					MyUser = User.objects.latest('id')
					request.session['user'] = MyUser.id
					MenuList = Menu.objects.order_by('number')
					SlideList = Slide.objects.order_by('number')
					context = {
						'MenuList': MenuList,
						'SlideList': SlideList,
					}
					return redirect('http://127.0.0.1:8000/')
			else:
				PassError = "The passwords don't match"
				return HttpResponse(PassError)
		else:
			PassError = "Not valid Form"
			return HttpResponse(PassError)




def RegProcessAfterSupportSelect(request,CompanyTypeSlug,SupportSlug):
	# if this is a POST request we need to process the form data
	if request.method == 'POST':
		# create a form instance and populate it with data from the request:
		form = RegForm(request.POST)
		# check whether it's valid:
		if form.is_valid():
			login = form.cleaned_data['login']
			CompanyName = form.cleaned_data['coname']
			CompanyRegion = form.cleaned_data['coregion']
			password = form.cleaned_data['password']
			passagain = form.cleaned_data['passagain']
			phone = form.cleaned_data['phone']
			mail = form.cleaned_data['email']

			prev = form.cleaned_data['prev']
			if(passagain == password):
				if User.objects.filter(login=login).exists():
					raise forms.ValidationError(u'Username "%s" is already in use.' % username)
				else:
					password = password.encode('utf-8')
					#password = hashlib.sha256(password)
					NewUser = User(login=login,CompanyName=CompanyName,CompanyRegion=CompanyRegion,password=password,PhoneNumber=phone,email=mail)
					NewUser.save()

					MyUser = User.objects.latest('id')
					request.session['user'] = MyUser.id
					
					return redirect('http://127.0.0.1:8000/services/'+CompanyTypeSlug+'/'+SupportSlug)
			else:
				PassError = "The passwords don't match"
				return HttpResponse(PassError)
		else:
			NotValdForm = "Not valid Form"
			return HttpResponse(NotValdForm)


	# template = loader.get_template('Auth/index1.html')
	# return HttpResponse(template.render(context, request))

# Create your views here.
