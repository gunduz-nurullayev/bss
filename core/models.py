from django.db import models

import os
from django.core.validators import URLValidator

# Create your models here.

class Menu(models.Model):				#Example: Home, About, Services..
    name = models.CharField(max_length=200)
    url = models.CharField(max_length=200)
    number = models.IntegerField()
    # pub_date = models.DateTimeField('date published')

class Slide(models.Model):						#Example: Slide 1, Slide 1..
    image = models.ImageField('Image', upload_to='uploads/slide/')
    title = models.CharField(max_length=200)
    description = models.TextField(max_length=100000)
    url = models.CharField(max_length=200)
    number = models.IntegerField()

class BsssImagination(models.Model):					#Example: Name, Text, Name2, Image
	name1 = models.CharField(max_length=200)
	name2 = models.CharField(max_length=200)
	text = models.TextField(max_length=100000)
	image = models.ImageField('Image', upload_to='uploads/imagination/')

class WWUsAdvantage(models.Model):					#Example: Gold, Silver, Bronze, Standart, Europe
	name = models.CharField(max_length=200)
	description = models.TextField(max_length=100000)
	image = models.ImageField('Image', upload_to='uploads/wwusadvantage/')

class HowDoesItWork(models.Model):						#Example: Corporate portal, Corporate portal + e-commerce..
	# Company = models.ForeignKey(Company, on_delete=models.CASCADE)
	name = models.CharField(max_length=200)
	text = models.TextField(max_length=10000)
	presentation = models.FileField(upload_to='uploads/howdoesitwork/presentation/')
	
	# #Additional info
	# PackageType = models.ForeignKey(PackageType, on_delete=models.CASCADE)
	# PackageRegion = models.ForeignKey(PackageRegion, on_delete=models.CASCADE)

class ContactUs(models.Model):						#Example: Name, Subject, E-mail, Text-message..
    fullname = models.CharField('Full Name', max_length=100, blank=False)
    subject = models.CharField('Subject', max_length=150,)
    company = models.CharField('Company', max_length=250, blank=True, null=True)
    email = models.EmailField('Email')
    phone = models.CharField('Phone', max_length=50, blank=False,null=True)
    message = models.TextField('Message')
    created = models.DateTimeField(auto_now_add=True, editable=False)

class Portfolio(models.Model):						#Example: Name, Subject, E-mail, Text-message..
	name = models.CharField(max_length=200)
	description = models.TextField(max_length=20000)
	category = models.CharField(max_length=100)
	url = models.CharField(max_length=300)
	image = models.ImageField(upload_to='uploads/portfolio/image/')

class PortfolioImages(models.Model):
    portfolio = models.ForeignKey(Portfolio, name='portfolio')
    image = models.ImageField(upload_to='uploads/portfolio/images/')

    def __unicode__(self):
        return self.portfolio

class Partner(models.Model):						#Example: Name, Subject, E-mail, Text-message..
	name = models.CharField(max_length=200)
	description = models.TextField(max_length=20000)
	url = models.CharField(max_length=300)
	image = models.ImageField(upload_to='uploads/partners/image/')


# class Services(models.Model):						#Example: Content management, Translate, Promotion..
# 	Package = models.ForeignKey(Package, on_delete=models.CASCADE)
# 	name = models.CharField(max_length=100)
# 	description = models.TextField(max_length=100000)
