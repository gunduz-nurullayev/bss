from django.shortcuts import render

from django.http import HttpResponse

from django.template import loader

from .models import Menu

from .models import Slide

from .models import BsssImagination

from .models import HowDoesItWork

from .models import WWUsAdvantage

from .models import Portfolio

from .models import PortfolioImages

from .models import Partner


# From other Apps'
from Services.models import CompanyCategory

from Services.models import CompanyType

# Create your views here.

from django.shortcuts import get_object_or_404, render

def index(request):							
	MyUser = 0
	if(request.session.get('user')):
		MyUser = request.session['user']

	MenuList = Menu.objects.order_by('number')

	SlideList = Slide.objects.order_by('number')

	BsssImaginationData = get_object_or_404(BsssImagination)


	# From other Apps'
	CompanyCategoryList = CompanyCategory.objects.order_by('number')
	CompanyTypeList = CompanyType.objects.order_by('id')
	WWUsAdvantageList = WWUsAdvantage.objects.order_by('id')

	PortfolioList = Portfolio.objects.order_by('id')
	PortfolioImagesList = PortfolioImages.objects.order_by('id')
	PartnerList = Partner.objects.order_by('id')

	HowDoesItWorkContent = get_object_or_404(HowDoesItWork)

	# CompanyCategories = ', '.join([ComCats.name for ComCats in CompanyCategories])

	context = {
		'MyUser':MyUser,
		'MenuList': MenuList,
		'SlideList': SlideList,
		'BsssImaginationData': BsssImaginationData,
		'CompanyCategoryList': CompanyCategoryList,
		'CompanyTypeList': CompanyTypeList,
		'WWUsAdvantageList': WWUsAdvantageList,
		'HowDoesItWorkContent': HowDoesItWorkContent,
		'PortfolioList': PortfolioList,
		'PortfolioImagesList': PortfolioImagesList,
		'PartnerList': PartnerList,
	}
	template = loader.get_template('core/index.html')
	return HttpResponse(template.render(context, request))