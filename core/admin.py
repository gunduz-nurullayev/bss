from django.contrib import admin

from .models import Menu,Slide,BsssImagination,WWUsAdvantage,HowDoesItWork,ContactUs,Portfolio,PortfolioImages,Partner

@admin.register(Menu)
class MenuAdmin(admin.ModelAdmin):
	list_display = ['name']


@admin.register(Slide)
class SlideAdmin(admin.ModelAdmin):
	list_display = ['title']


@admin.register(BsssImagination)
class BsssImaginationAdmin(admin.ModelAdmin):
	list_display = ['name1']


@admin.register(WWUsAdvantage)
class WWUsAdvantageAdmin(admin.ModelAdmin):
	list_display = ['name']


@admin.register(HowDoesItWork)
class HowDoesItWorkAdmin(admin.ModelAdmin):
	list_display = ['name']


@admin.register(ContactUs)
class ContactUsAdmin(admin.ModelAdmin):
	list_display = ['id']


@admin.register(PortfolioImages)
class PortfolioImagesInline(admin.ModelAdmin):
    list_display = ['id']


@admin.register(Portfolio)
class PortfolioAdmin(admin.ModelAdmin):
	list_display = ['name']

@admin.register(Partner)
class PartnerAdmin(admin.ModelAdmin):
	list_display = ['name']
# Register your models here.
