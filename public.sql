/*
Navicat PGSQL Data Transfer

Source Server         : postgresql
Source Server Version : 90506
Source Host           : localhost:5432
Source Database       : bsss
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90506
File Encoding         : 65001

Date: 2017-05-11 16:30:31
*/


-- ----------------------------
-- Sequence structure for Auth_additionalservicelist_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."Auth_additionalservicelist_id_seq";
CREATE SEQUENCE "public"."Auth_additionalservicelist_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 7296
 CACHE 1;
SELECT setval('"public"."Auth_additionalservicelist_id_seq"', 7296, true);

-- ----------------------------
-- Sequence structure for auth_group_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."auth_group_id_seq";
CREATE SEQUENCE "public"."auth_group_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for auth_group_permissions_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."auth_group_permissions_id_seq";
CREATE SEQUENCE "public"."auth_group_permissions_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for Auth_order_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."Auth_order_id_seq";
CREATE SEQUENCE "public"."Auth_order_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 12
 CACHE 1;
SELECT setval('"public"."Auth_order_id_seq"', 12, true);

-- ----------------------------
-- Sequence structure for auth_permission_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."auth_permission_id_seq";
CREATE SEQUENCE "public"."auth_permission_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 90
 CACHE 1;
SELECT setval('"public"."auth_permission_id_seq"', 90, true);

-- ----------------------------
-- Sequence structure for Auth_projectrecommendation_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."Auth_projectrecommendation_id_seq";
CREATE SEQUENCE "public"."Auth_projectrecommendation_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 11
 CACHE 1;
SELECT setval('"public"."Auth_projectrecommendation_id_seq"', 11, true);

-- ----------------------------
-- Sequence structure for auth_user_groups_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."auth_user_groups_id_seq";
CREATE SEQUENCE "public"."auth_user_groups_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for auth_user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."auth_user_id_seq";
CREATE SEQUENCE "public"."auth_user_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."auth_user_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for Auth_user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."Auth_user_id_seq";
CREATE SEQUENCE "public"."Auth_user_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 30
 CACHE 1;
SELECT setval('"public"."Auth_user_id_seq"', 30, true);

-- ----------------------------
-- Sequence structure for auth_user_user_permissions_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."auth_user_user_permissions_id_seq";
CREATE SEQUENCE "public"."auth_user_user_permissions_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for core_bsssimagination_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."core_bsssimagination_id_seq";
CREATE SEQUENCE "public"."core_bsssimagination_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."core_bsssimagination_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for core_contactus_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."core_contactus_id_seq";
CREATE SEQUENCE "public"."core_contactus_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for core_howdoesitwork_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."core_howdoesitwork_id_seq";
CREATE SEQUENCE "public"."core_howdoesitwork_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"public"."core_howdoesitwork_id_seq"', 1, true);

-- ----------------------------
-- Sequence structure for core_menu_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."core_menu_id_seq";
CREATE SEQUENCE "public"."core_menu_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 7
 CACHE 1;
SELECT setval('"public"."core_menu_id_seq"', 7, true);

-- ----------------------------
-- Sequence structure for core_partners_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."core_partners_id_seq";
CREATE SEQUENCE "public"."core_partners_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;
SELECT setval('"public"."core_partners_id_seq"', 2, true);

-- ----------------------------
-- Sequence structure for core_portfolio_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."core_portfolio_id_seq";
CREATE SEQUENCE "public"."core_portfolio_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 5
 CACHE 1;
SELECT setval('"public"."core_portfolio_id_seq"', 5, true);

-- ----------------------------
-- Sequence structure for core_portfolioimages_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."core_portfolioimages_id_seq";
CREATE SEQUENCE "public"."core_portfolioimages_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 3
 CACHE 1;
SELECT setval('"public"."core_portfolioimages_id_seq"', 3, true);

-- ----------------------------
-- Sequence structure for core_slide_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."core_slide_id_seq";
CREATE SEQUENCE "public"."core_slide_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 18
 CACHE 1;
SELECT setval('"public"."core_slide_id_seq"', 18, true);

-- ----------------------------
-- Sequence structure for core_wwusadvantage_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."core_wwusadvantage_id_seq";
CREATE SEQUENCE "public"."core_wwusadvantage_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 3
 CACHE 1;
SELECT setval('"public"."core_wwusadvantage_id_seq"', 3, true);

-- ----------------------------
-- Sequence structure for django_admin_log_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."django_admin_log_id_seq";
CREATE SEQUENCE "public"."django_admin_log_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 180
 CACHE 1;
SELECT setval('"public"."django_admin_log_id_seq"', 180, true);

-- ----------------------------
-- Sequence structure for django_content_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."django_content_type_id_seq";
CREATE SEQUENCE "public"."django_content_type_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 31
 CACHE 1;
SELECT setval('"public"."django_content_type_id_seq"', 31, true);

-- ----------------------------
-- Sequence structure for django_migrations_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."django_migrations_id_seq";
CREATE SEQUENCE "public"."django_migrations_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 40
 CACHE 1;
SELECT setval('"public"."django_migrations_id_seq"', 40, true);

-- ----------------------------
-- Sequence structure for Services_companycategory_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."Services_companycategory_id_seq";
CREATE SEQUENCE "public"."Services_companycategory_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;
SELECT setval('"public"."Services_companycategory_id_seq"', 2, true);

-- ----------------------------
-- Sequence structure for Services_companytype_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."Services_companytype_id_seq";
CREATE SEQUENCE "public"."Services_companytype_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 3
 CACHE 1;
SELECT setval('"public"."Services_companytype_id_seq"', 3, true);

-- ----------------------------
-- Sequence structure for Services_service_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."Services_service_id_seq";
CREATE SEQUENCE "public"."Services_service_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 8
 CACHE 1;
SELECT setval('"public"."Services_service_id_seq"', 8, true);

-- ----------------------------
-- Sequence structure for Services_support_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."Services_support_id_seq";
CREATE SEQUENCE "public"."Services_support_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 7
 CACHE 1;
SELECT setval('"public"."Services_support_id_seq"', 7, true);

-- ----------------------------
-- Sequence structure for Services_supportadvantage_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."Services_supportadvantage_id_seq";
CREATE SEQUENCE "public"."Services_supportadvantage_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 2
 CACHE 1;
SELECT setval('"public"."Services_supportadvantage_id_seq"', 2, true);

-- ----------------------------
-- Table structure for Auth_additionalservicelist
-- ----------------------------
DROP TABLE IF EXISTS "public"."Auth_additionalservicelist";
CREATE TABLE "public"."Auth_additionalservicelist" (
"id" int4 DEFAULT nextval('"Auth_additionalservicelist_id_seq"'::regclass) NOT NULL,
"Order_id" int4 NOT NULL,
"Service_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of Auth_additionalservicelist
-- ----------------------------
INSERT INTO "public"."Auth_additionalservicelist" VALUES ('7294', '10', '7');
INSERT INTO "public"."Auth_additionalservicelist" VALUES ('7295', '12', '3');
INSERT INTO "public"."Auth_additionalservicelist" VALUES ('7296', '12', '3');

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS "public"."auth_group";
CREATE TABLE "public"."auth_group" (
"id" int4 DEFAULT nextval('auth_group_id_seq'::regclass) NOT NULL,
"name" varchar(80) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of auth_group
-- ----------------------------

-- ----------------------------
-- Table structure for auth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS "public"."auth_group_permissions";
CREATE TABLE "public"."auth_group_permissions" (
"id" int4 DEFAULT nextval('auth_group_permissions_id_seq'::regclass) NOT NULL,
"group_id" int4 NOT NULL,
"permission_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of auth_group_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for Auth_order
-- ----------------------------
DROP TABLE IF EXISTS "public"."Auth_order";
CREATE TABLE "public"."Auth_order" (
"id" int4 DEFAULT nextval('"Auth_order_id_seq"'::regclass) NOT NULL,
"Support_id" int4 NOT NULL,
"User_id" int4 NOT NULL,
"price" int4 NOT NULL,
"slug" varchar(200) COLLATE "default" NOT NULL,
"CompanyCustomers" text COLLATE "default" NOT NULL,
"CompanyProduction" text COLLATE "default" NOT NULL,
"WorkRegions" text COLLATE "default" NOT NULL,
"Name" varchar(200) COLLATE "default" NOT NULL,
"OrderLevel" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of Auth_order
-- ----------------------------
INSERT INTO "public"."Auth_order" VALUES ('10', '3', '27', '0', 'personnel-management', 'Usual people', 'We produce some type of Energy Drinks', 'Whole World', 'Personnel Management', '3');
INSERT INTO "public"."Auth_order" VALUES ('11', '3', '28', '0', 'personnel-management', 'Our customers are textile fans.', 'My Production is about textile.', 'Asia', 'Personnel Management', '1');
INSERT INTO "public"."Auth_order" VALUES ('12', '3', '29', '0', 'personnel-management', 'Water producers', 'Our products are water bottles', 'Whole World', 'Personnel Management', '1');

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS "public"."auth_permission";
CREATE TABLE "public"."auth_permission" (
"id" int4 DEFAULT nextval('auth_permission_id_seq'::regclass) NOT NULL,
"name" varchar(255) COLLATE "default" NOT NULL,
"content_type_id" int4 NOT NULL,
"codename" varchar(100) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO "public"."auth_permission" VALUES ('1', 'Can add log entry', '1', 'add_logentry');
INSERT INTO "public"."auth_permission" VALUES ('2', 'Can change log entry', '1', 'change_logentry');
INSERT INTO "public"."auth_permission" VALUES ('3', 'Can delete log entry', '1', 'delete_logentry');
INSERT INTO "public"."auth_permission" VALUES ('4', 'Can add permission', '2', 'add_permission');
INSERT INTO "public"."auth_permission" VALUES ('5', 'Can change permission', '2', 'change_permission');
INSERT INTO "public"."auth_permission" VALUES ('6', 'Can delete permission', '2', 'delete_permission');
INSERT INTO "public"."auth_permission" VALUES ('7', 'Can add group', '3', 'add_group');
INSERT INTO "public"."auth_permission" VALUES ('8', 'Can change group', '3', 'change_group');
INSERT INTO "public"."auth_permission" VALUES ('9', 'Can delete group', '3', 'delete_group');
INSERT INTO "public"."auth_permission" VALUES ('10', 'Can add user', '4', 'add_user');
INSERT INTO "public"."auth_permission" VALUES ('11', 'Can change user', '4', 'change_user');
INSERT INTO "public"."auth_permission" VALUES ('12', 'Can delete user', '4', 'delete_user');
INSERT INTO "public"."auth_permission" VALUES ('13', 'Can add content type', '5', 'add_contenttype');
INSERT INTO "public"."auth_permission" VALUES ('14', 'Can change content type', '5', 'change_contenttype');
INSERT INTO "public"."auth_permission" VALUES ('15', 'Can delete content type', '5', 'delete_contenttype');
INSERT INTO "public"."auth_permission" VALUES ('16', 'Can add session', '6', 'add_session');
INSERT INTO "public"."auth_permission" VALUES ('17', 'Can change session', '6', 'change_session');
INSERT INTO "public"."auth_permission" VALUES ('18', 'Can delete session', '6', 'delete_session');
INSERT INTO "public"."auth_permission" VALUES ('19', 'Can add service', '7', 'add_service');
INSERT INTO "public"."auth_permission" VALUES ('20', 'Can change service', '7', 'change_service');
INSERT INTO "public"."auth_permission" VALUES ('21', 'Can delete service', '7', 'delete_service');
INSERT INTO "public"."auth_permission" VALUES ('22', 'Can add package', '8', 'add_package');
INSERT INTO "public"."auth_permission" VALUES ('23', 'Can change package', '8', 'change_package');
INSERT INTO "public"."auth_permission" VALUES ('24', 'Can delete package', '8', 'delete_package');
INSERT INTO "public"."auth_permission" VALUES ('25', 'Can add package line', '9', 'add_packageline');
INSERT INTO "public"."auth_permission" VALUES ('26', 'Can change package line', '9', 'change_packageline');
INSERT INTO "public"."auth_permission" VALUES ('27', 'Can delete package line', '9', 'delete_packageline');
INSERT INTO "public"."auth_permission" VALUES ('28', 'Can add company category', '10', 'add_companycategory');
INSERT INTO "public"."auth_permission" VALUES ('29', 'Can change company category', '10', 'change_companycategory');
INSERT INTO "public"."auth_permission" VALUES ('30', 'Can delete company category', '10', 'delete_companycategory');
INSERT INTO "public"."auth_permission" VALUES ('31', 'Can add package', '11', 'add_package');
INSERT INTO "public"."auth_permission" VALUES ('32', 'Can change package', '11', 'change_package');
INSERT INTO "public"."auth_permission" VALUES ('33', 'Can delete package', '11', 'delete_package');
INSERT INTO "public"."auth_permission" VALUES ('34', 'Can add company', '12', 'add_company');
INSERT INTO "public"."auth_permission" VALUES ('35', 'Can change company', '12', 'change_company');
INSERT INTO "public"."auth_permission" VALUES ('36', 'Can delete company', '12', 'delete_company');
INSERT INTO "public"."auth_permission" VALUES ('37', 'Can add package category', '13', 'add_packagecategory');
INSERT INTO "public"."auth_permission" VALUES ('38', 'Can change package category', '13', 'change_packagecategory');
INSERT INTO "public"."auth_permission" VALUES ('39', 'Can delete package category', '13', 'delete_packagecategory');
INSERT INTO "public"."auth_permission" VALUES ('40', 'Can add services', '14', 'add_services');
INSERT INTO "public"."auth_permission" VALUES ('41', 'Can change services', '14', 'change_services');
INSERT INTO "public"."auth_permission" VALUES ('42', 'Can delete services', '14', 'delete_services');
INSERT INTO "public"."auth_permission" VALUES ('43', 'Can add package type', '15', 'add_packagetype');
INSERT INTO "public"."auth_permission" VALUES ('44', 'Can change package type', '15', 'change_packagetype');
INSERT INTO "public"."auth_permission" VALUES ('45', 'Can delete package type', '15', 'delete_packagetype');
INSERT INTO "public"."auth_permission" VALUES ('46', 'Can add package region', '16', 'add_packageregion');
INSERT INTO "public"."auth_permission" VALUES ('47', 'Can change package region', '16', 'change_packageregion');
INSERT INTO "public"."auth_permission" VALUES ('48', 'Can delete package region', '16', 'delete_packageregion');
INSERT INTO "public"."auth_permission" VALUES ('49', 'Can add bsss imagination', '17', 'add_bsssimagination');
INSERT INTO "public"."auth_permission" VALUES ('50', 'Can change bsss imagination', '17', 'change_bsssimagination');
INSERT INTO "public"."auth_permission" VALUES ('51', 'Can delete bsss imagination', '17', 'delete_bsssimagination');
INSERT INTO "public"."auth_permission" VALUES ('52', 'Can add contact us', '18', 'add_contactus');
INSERT INTO "public"."auth_permission" VALUES ('53', 'Can change contact us', '18', 'change_contactus');
INSERT INTO "public"."auth_permission" VALUES ('54', 'Can delete contact us', '18', 'delete_contactus');
INSERT INTO "public"."auth_permission" VALUES ('55', 'Can add how does it work', '19', 'add_howdoesitwork');
INSERT INTO "public"."auth_permission" VALUES ('56', 'Can change how does it work', '19', 'change_howdoesitwork');
INSERT INTO "public"."auth_permission" VALUES ('57', 'Can delete how does it work', '19', 'delete_howdoesitwork');
INSERT INTO "public"."auth_permission" VALUES ('58', 'Can add menu', '20', 'add_menu');
INSERT INTO "public"."auth_permission" VALUES ('59', 'Can change menu', '20', 'change_menu');
INSERT INTO "public"."auth_permission" VALUES ('60', 'Can delete menu', '20', 'delete_menu');
INSERT INTO "public"."auth_permission" VALUES ('61', 'Can add slide', '21', 'add_slide');
INSERT INTO "public"."auth_permission" VALUES ('62', 'Can change slide', '21', 'change_slide');
INSERT INTO "public"."auth_permission" VALUES ('63', 'Can delete slide', '21', 'delete_slide');
INSERT INTO "public"."auth_permission" VALUES ('64', 'Can add ww us advantage', '22', 'add_wwusadvantage');
INSERT INTO "public"."auth_permission" VALUES ('65', 'Can change ww us advantage', '22', 'change_wwusadvantage');
INSERT INTO "public"."auth_permission" VALUES ('66', 'Can delete ww us advantage', '22', 'delete_wwusadvantage');
INSERT INTO "public"."auth_permission" VALUES ('67', 'Can add company type', '23', 'add_companytype');
INSERT INTO "public"."auth_permission" VALUES ('68', 'Can change company type', '23', 'change_companytype');
INSERT INTO "public"."auth_permission" VALUES ('69', 'Can delete company type', '23', 'delete_companytype');
INSERT INTO "public"."auth_permission" VALUES ('70', 'Can add service', '24', 'add_service');
INSERT INTO "public"."auth_permission" VALUES ('71', 'Can change service', '24', 'change_service');
INSERT INTO "public"."auth_permission" VALUES ('72', 'Can delete service', '24', 'delete_service');
INSERT INTO "public"."auth_permission" VALUES ('73', 'Can add support', '25', 'add_support');
INSERT INTO "public"."auth_permission" VALUES ('74', 'Can change support', '25', 'change_support');
INSERT INTO "public"."auth_permission" VALUES ('75', 'Can delete support', '25', 'delete_support');
INSERT INTO "public"."auth_permission" VALUES ('76', 'Can add support advantage', '26', 'add_supportadvantage');
INSERT INTO "public"."auth_permission" VALUES ('77', 'Can change support advantage', '26', 'change_supportadvantage');
INSERT INTO "public"."auth_permission" VALUES ('78', 'Can delete support advantage', '26', 'delete_supportadvantage');
INSERT INTO "public"."auth_permission" VALUES ('79', 'Can add portfolio', '27', 'add_portfolio');
INSERT INTO "public"."auth_permission" VALUES ('80', 'Can change portfolio', '27', 'change_portfolio');
INSERT INTO "public"."auth_permission" VALUES ('81', 'Can delete portfolio', '27', 'delete_portfolio');
INSERT INTO "public"."auth_permission" VALUES ('82', 'Can add portfolio images', '28', 'add_portfolioimages');
INSERT INTO "public"."auth_permission" VALUES ('83', 'Can change portfolio images', '28', 'change_portfolioimages');
INSERT INTO "public"."auth_permission" VALUES ('84', 'Can delete portfolio images', '28', 'delete_portfolioimages');
INSERT INTO "public"."auth_permission" VALUES ('85', 'Can add partners', '29', 'add_partners');
INSERT INTO "public"."auth_permission" VALUES ('86', 'Can change partners', '29', 'change_partners');
INSERT INTO "public"."auth_permission" VALUES ('87', 'Can delete partners', '29', 'delete_partners');
INSERT INTO "public"."auth_permission" VALUES ('88', 'Can add partner', '29', 'add_partner');
INSERT INTO "public"."auth_permission" VALUES ('89', 'Can change partner', '29', 'change_partner');
INSERT INTO "public"."auth_permission" VALUES ('90', 'Can delete partner', '29', 'delete_partner');

-- ----------------------------
-- Table structure for Auth_projectrecommendation
-- ----------------------------
DROP TABLE IF EXISTS "public"."Auth_projectrecommendation";
CREATE TABLE "public"."Auth_projectrecommendation" (
"id" int4 DEFAULT nextval('"Auth_projectrecommendation_id_seq"'::regclass) NOT NULL,
"Justification" text COLLATE "default" NOT NULL,
"Order_id" int4 NOT NULL,
"Service_id" int4 NOT NULL,
"User_id" int4 NOT NULL,
"Apply" bool NOT NULL,
"Author" int4 NOT NULL,
"read" bool NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of Auth_projectrecommendation
-- ----------------------------
INSERT INTO "public"."Auth_projectrecommendation" VALUES ('9', '<p>The Finance Division on Tuesday issued a revised budget authority which cut down the FY2015-16 budget outlay by 10.35%.</p>

<p>The revised budget outlay would be Tk264,565 crore, down by Tk30,535 crore from the original Tk295,100 crore budget outlay.</p>

<p>Senior Finance Secretary Mahbub Ahmed said that Finance Minister AMA Muhith would place the revised budget outlay to the current parliament session on May 2.</p>

<p>The budget outlay has been shrunk because the previous development funds could not be utilized.</p>

<p>One of the major issues for downsizing was the reduction of revenue collection by Tk26,370 crore from the actual outlay of Tk176,370 crore.</p>

<p>Mahbub said the shortage of revenue collection by the National Board of Revenue would also have a major impact on the total budget for the next fiscal year.The Finance Division on Tuesday issued a revised budget authority which cut down the FY2015-16 budget outlay by 10.35%.</p>

<p>The revised budget outlay would be Tk264,565 crore, down by Tk30,535 crore from the original Tk295,100 crore budget outlay.</p>

<p>Senior Finance Secretary Mahbub Ahmed said that Finance Minister AMA Muhith would place the revised budget outlay to the current parliament session on May 2.</p>

<p>The budget outlay has been shrunk because the previous development funds could not be utilized.</p>

<p>One of the major issues for downsizing was the reduction of revenue collection by Tk26,370 crore from the actual outlay of Tk176,370 crore.</p>

<p>Mahbub said the shortage of revenue collection by the National Board of Revenue would also have a major impact on the total budget for the next fiscal year.</p>', '10', '6', '29', 'f', '1', 'f');
INSERT INTO "public"."Auth_projectrecommendation" VALUES ('10', '<p>The related policies and procedures govern activities such as the selection, planning, and acquisition of medical devices, and the inspection, acceptance, maintenance, and eventual retirement and disposal of medical equipment. Medical equipment management is a recognized profession within the medical logistics domain.</p>

<p>Govern activities such as the selection, planning, and acquisition of medical devices, and the inspection, acceptance, maintenance, and eventual retirement and disposal of medical equipment. Medical equipment management is a recognized profession within the medical logistics domain.</p>

<p>The related policies and procedures govern activities such as the selection, planning, and acquisition of medical devices, and the inspection, acceptance, maintenance, and eventual retirement and disposal of medical equipment. Medical equipment management is a recognized profession within the medical logistics.</p>', '10', '7', '29', 't', '1', 'f');
INSERT INTO "public"."Auth_projectrecommendation" VALUES ('11', '<p>A business manager is sought after by a variety of companies in many industries. Some employers will be looking for candidates with a bachelor&#39;s degree in business administration or a similar discipline, while others are willing to accept an applicant with a high school diploma or associate&#39;s degree. The business manager should also usually have at least two years of experience working in administration positions, in addition to several years of experience in the industry of the company. The prospective employee should be proficient in Microsoft Office. The business manager will primarily work indoors, although the settings may vary. There can be some travel involved.</p>', '12', '3', '29', 't', '1', 'f');

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."auth_user";
CREATE TABLE "public"."auth_user" (
"id" int4 DEFAULT nextval('auth_user_id_seq'::regclass) NOT NULL,
"password" varchar(128) COLLATE "default" NOT NULL,
"last_login" timestamptz(6),
"is_superuser" bool NOT NULL,
"username" varchar(150) COLLATE "default" NOT NULL,
"first_name" varchar(30) COLLATE "default" NOT NULL,
"last_name" varchar(30) COLLATE "default" NOT NULL,
"email" varchar(254) COLLATE "default" NOT NULL,
"is_staff" bool NOT NULL,
"is_active" bool NOT NULL,
"date_joined" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of auth_user
-- ----------------------------
INSERT INTO "public"."auth_user" VALUES ('1', 'pbkdf2_sha256$36000$xVaMdL8X6H3p$oRC6Vo+sPM8ZfCF2Uw/sD3hwyaxT9Vf/gVb/Wdf319M=', '2017-05-01 06:02:18.636686-07', 't', '_bsss', '', '', 'developer.turqay.ali@gmail.com', 't', 't', '2017-04-14 04:01:57.735361-07');

-- ----------------------------
-- Table structure for Auth_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."Auth_user";
CREATE TABLE "public"."Auth_user" (
"id" int4 DEFAULT nextval('"Auth_user_id_seq"'::regclass) NOT NULL,
"CompanyName" varchar(200) COLLATE "default" NOT NULL,
"CompanyRegion" varchar(200) COLLATE "default" NOT NULL,
"login" varchar(200) COLLATE "default" NOT NULL,
"password" varchar(2000) COLLATE "default" NOT NULL,
"PhoneNumber" int8 NOT NULL,
"email" varchar(254) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of Auth_user
-- ----------------------------
INSERT INTO "public"."Auth_user" VALUES ('27', 'Chagdash', 'Baku', 'Elnur', 'dev1106', '994506141777', 'developer.turqay.ali@gmail.com');
INSERT INTO "public"."Auth_user" VALUES ('28', 'Chagdash', 'Europe', 'Factories', 'dev1106', '994506141777', 'developer.turqay.ali@gmail.com');
INSERT INTO "public"."Auth_user" VALUES ('29', 'Chagdash', 'Whole World', 'Turqay', 'dev1106', '994506141777', 'developer.turqay.ali@gmail.com');
INSERT INTO "public"."Auth_user" VALUES ('30', 'Chagdash', 'Canada', 'Developer', 'dev1106', '994506141777', 'developer.turqay.ali@gmail.com');

-- ----------------------------
-- Table structure for auth_user_groups
-- ----------------------------
DROP TABLE IF EXISTS "public"."auth_user_groups";
CREATE TABLE "public"."auth_user_groups" (
"id" int4 DEFAULT nextval('auth_user_groups_id_seq'::regclass) NOT NULL,
"user_id" int4 NOT NULL,
"group_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of auth_user_groups
-- ----------------------------

-- ----------------------------
-- Table structure for auth_user_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS "public"."auth_user_user_permissions";
CREATE TABLE "public"."auth_user_user_permissions" (
"id" int4 DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass) NOT NULL,
"user_id" int4 NOT NULL,
"permission_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of auth_user_user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for core_bsssimagination
-- ----------------------------
DROP TABLE IF EXISTS "public"."core_bsssimagination";
CREATE TABLE "public"."core_bsssimagination" (
"id" int4 DEFAULT nextval('core_bsssimagination_id_seq'::regclass) NOT NULL,
"name1" varchar(200) COLLATE "default" NOT NULL,
"name2" varchar(200) COLLATE "default" NOT NULL,
"text" text COLLATE "default" NOT NULL,
"image" varchar(100) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of core_bsssimagination
-- ----------------------------
INSERT INTO "public"."core_bsssimagination" VALUES ('1', 'Bussiness Support Services', 'Bussiness Support Services Project', 'Piyasada yönetimi ve tasarım gelişmiş teknolojilerin tanıtımı ile, özel şirketlerin hizmetleri için ihtiyaç vardı. Şirketimizin amacı, yerel ve uluslararası pazarlarda çeşitli sektörlerde bilişim teknolojileri uygulamasıdır.
3SC Universus inşaat, inşaat tasarım, mobilya ve inşaat malzemeleri, e-ticaret ve diğer çözümleri çeşitli sektörlerde yaratma deneyime sahip olan yüksek nitelikli idari ve teknik personeli tarafından kurulub.

Uzmanlarımız etkili, verimli ve kaliteli müşterilerimize karşı karşıya herhangi görevleri çöze biliyorlar.

3SC Universus ortaklarının ve müşterilerinin rekabet gücünü artırmak için katkıda iş bilgi teknolojisi alanında geniş bir deneyime sahiptir. Bunun kanıtı, son yıllarda oluşturulan ve bizim tarafımızdan uygulanmaktadır, inşaat sektöründe birçok etkili ve parlak çözümler.', 'uploads/imagination/BsssSystem.png');

-- ----------------------------
-- Table structure for core_contactus
-- ----------------------------
DROP TABLE IF EXISTS "public"."core_contactus";
CREATE TABLE "public"."core_contactus" (
"id" int4 DEFAULT nextval('core_contactus_id_seq'::regclass) NOT NULL,
"fullname" varchar(100) COLLATE "default" NOT NULL,
"subject" varchar(150) COLLATE "default" NOT NULL,
"company" varchar(250) COLLATE "default",
"email" varchar(254) COLLATE "default" NOT NULL,
"message" text COLLATE "default" NOT NULL,
"created" timestamptz(6) NOT NULL,
"phone" varchar(50) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of core_contactus
-- ----------------------------

-- ----------------------------
-- Table structure for core_howdoesitwork
-- ----------------------------
DROP TABLE IF EXISTS "public"."core_howdoesitwork";
CREATE TABLE "public"."core_howdoesitwork" (
"id" int4 DEFAULT nextval('core_howdoesitwork_id_seq'::regclass) NOT NULL,
"name" varchar(200) COLLATE "default" NOT NULL,
"text" text COLLATE "default" NOT NULL,
"presentation" varchar(100) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of core_howdoesitwork
-- ----------------------------
INSERT INTO "public"."core_howdoesitwork" VALUES ('1', 'How does it works', 'С внедрением на рынке передовых технологий управления и проектирования, появилась надобность в услугах специализированных компаний. Целью этой компании должно быть сосредоточение на внедрении высоких технологий в области использования информационных технологий в различных отраслях на местных и международных рынках.
3SC universus создана из высококвалифицированных управленческих кадров и инженерно-технического персонала за плечами которых беспрецедентный опыт создания высокотехнологических интернет-решений для различных отраслей, таких как строительство, строительное проектирование, электронная торговля мебели и стройматериалами и других.', 'uploads/howdoesitwork/presentation/Wildlife.mp4');

-- ----------------------------
-- Table structure for core_menu
-- ----------------------------
DROP TABLE IF EXISTS "public"."core_menu";
CREATE TABLE "public"."core_menu" (
"id" int4 DEFAULT nextval('core_menu_id_seq'::regclass) NOT NULL,
"name" varchar(200) COLLATE "default" NOT NULL,
"url" varchar(200) COLLATE "default" NOT NULL,
"number" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of core_menu
-- ----------------------------
INSERT INTO "public"."core_menu" VALUES ('1', 'Home', '/', '0');
INSERT INTO "public"."core_menu" VALUES ('2', 'Services', '/services', '1');
INSERT INTO "public"."core_menu" VALUES ('4', 'Portfolio', '#portfolio', '2');
INSERT INTO "public"."core_menu" VALUES ('5', 'Contact', '/contact', '6');
INSERT INTO "public"."core_menu" VALUES ('6', 'About', '/about', '3');
INSERT INTO "public"."core_menu" VALUES ('7', 'Published', '/published', '4');

-- ----------------------------
-- Table structure for core_partner
-- ----------------------------
DROP TABLE IF EXISTS "public"."core_partner";
CREATE TABLE "public"."core_partner" (
"id" int4 DEFAULT nextval('core_partners_id_seq'::regclass) NOT NULL,
"name" varchar(200) COLLATE "default" NOT NULL,
"description" text COLLATE "default" NOT NULL,
"url" varchar(300) COLLATE "default" NOT NULL,
"image" varchar(100) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of core_partner
-- ----------------------------
INSERT INTO "public"."core_partner" VALUES ('1', '3sc-Software', 'Наши специалисты способны решать задачи любой сложности стоящие перед нашими клиентами с присущей нам ЭФФЕКТИВНОСТЬЮ, РАЦИОНАЛЬНОСТЬЮ И КАЧЕСТВОМ.

3SC universus обладает богатым опытом в сфере внедрения информационных технологий в бизнесе, способствующих повысить конкурентоспособность своих партнеров и клиентов. Подтверждением этому служат созданные и реализованные нами за последние годы множество эффективных и ярких решений в строительной сфере.', 'http://3sc-software.com/ru/', 'uploads/partners/image/news.jpg');
INSERT INTO "public"."core_partner" VALUES ('2', '3sc-Software Commerce', 'Наши специалисты способны решать задачи любой сложности стоящие перед нашими клиентами с присущей нам ЭФФЕКТИВНОСТЬЮ, РАЦИОНАЛЬНОСТЬЮ И КАЧЕСТВОМ.

3SC universus обладает богатым опытом в сфере внедрения информационных технологий в бизнесе, способствующих повысить конкурентоспособность своих партнеров и клиентов. Подтверждением этому служат созданные и реализованные нами за последние годы множество эффективных и ярких решений в строительной сфере.', 'http://commerce.3sc-software.com/ru/', 'uploads/partners/image/news1.jpg');

-- ----------------------------
-- Table structure for core_portfolio
-- ----------------------------
DROP TABLE IF EXISTS "public"."core_portfolio";
CREATE TABLE "public"."core_portfolio" (
"id" int4 DEFAULT nextval('core_portfolio_id_seq'::regclass) NOT NULL,
"name" varchar(200) COLLATE "default" NOT NULL,
"description" text COLLATE "default" NOT NULL,
"url" varchar(300) COLLATE "default" NOT NULL,
"image" varchar(100) COLLATE "default" NOT NULL,
"category" varchar(100) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of core_portfolio
-- ----------------------------
INSERT INTO "public"."core_portfolio" VALUES ('2', '3 SC Universus Business', 'We are highly experienced developers, having built many industrial portals and corporate solutions. Seek success, seek solutions! Our team can provide you with a creative web solution customized to your needs. From a simple project needing database integration to a dynamic web portal using with numerous custom features, can meet your business goals with the perfect web based solution.', 'http://3sc-universus.com/en/', 'uploads/portfolio/image/3scUniversus_fem7fGi.png', 'Business');
INSERT INTO "public"."core_portfolio" VALUES ('3', '3 SC Universus', 'We are highly experienced developers, having built many industrial portals and corporate solutions. Seek success, seek solutions! Our team can provide you with a creative web solution customized to your needs. From a simple project needing database integration to a dynamic web portal using with numerous custom features, can meet your business goals with the perfect web based solution.http://3sc-universus.com/en/', 'http://3sc-universus.com/en/', 'uploads/portfolio/image/3scUniversus_2rfCqVS.png', 'Construction');
INSERT INTO "public"."core_portfolio" VALUES ('4', '3 SC Universus', 'We are highly experienced developers, having built many industrial portals and corporate solutions. Seek success, seek solutions! Our team can provide you with a creative web solution customized to your needs. From a simple project needing database integration to a dynamic web portal using with numerous custom features, can meet your business goals with the perfect web based solution.', 'http://3sc-universus.com/en/', 'uploads/portfolio/image/3scUniversus_OKPk9rE.png', 'Tourism');
INSERT INTO "public"."core_portfolio" VALUES ('5', '3 SC Universus', 'We are highly experienced developers, having built many industrial portals and corporate solutions. Seek success, seek solutions! Our team can provide you with a creative web solution customized to your needs. From a simple project needing database integration to a dynamic web portal using with numerous custom features, can meet your business goals with the perfect web based solution.', 'http://3sc-universus.com/en/', 'uploads/portfolio/image/3scUniversus_mUVo9iD.png', 'Textile');

-- ----------------------------
-- Table structure for core_portfolioimages
-- ----------------------------
DROP TABLE IF EXISTS "public"."core_portfolioimages";
CREATE TABLE "public"."core_portfolioimages" (
"id" int4 DEFAULT nextval('core_portfolioimages_id_seq'::regclass) NOT NULL,
"image" varchar(100) COLLATE "default" NOT NULL,
"portfolio_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of core_portfolioimages
-- ----------------------------
INSERT INTO "public"."core_portfolioimages" VALUES ('1', 'uploads/portfolio/images/meeting.jpg', '2');
INSERT INTO "public"."core_portfolioimages" VALUES ('2', 'uploads/portfolio/images/image.jpg', '2');
INSERT INTO "public"."core_portfolioimages" VALUES ('3', 'uploads/portfolio/images/news.jpg', '2');

-- ----------------------------
-- Table structure for core_slide
-- ----------------------------
DROP TABLE IF EXISTS "public"."core_slide";
CREATE TABLE "public"."core_slide" (
"id" int4 DEFAULT nextval('core_slide_id_seq'::regclass) NOT NULL,
"image" varchar(100) COLLATE "default" NOT NULL,
"title" varchar(200) COLLATE "default" NOT NULL,
"description" text COLLATE "default" NOT NULL,
"url" varchar(200) COLLATE "default" NOT NULL,
"number" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of core_slide
-- ----------------------------
INSERT INTO "public"."core_slide" VALUES ('1', 'uploads/slide/1.jpg', 'Bussiness Support Services', 'Biz şirketinizin online çözümleri için hizmetler ve teknik destek sunuyoruz', '/bss', '0');
INSERT INTO "public"."core_slide" VALUES ('2', 'uploads/slide/2.jpg', 'Less Personnel More Profit', 'Using less personnel provides yo gain more Profit.', '/lesspersonnel', '1');
INSERT INTO "public"."core_slide" VALUES ('4', 'uploads/slide/3_nse6rfy.jpg', 'Bsss Advantages', 'Bsss provides to customers number of Advantages', '/advantages', '2');

-- ----------------------------
-- Table structure for core_wwusadvantage
-- ----------------------------
DROP TABLE IF EXISTS "public"."core_wwusadvantage";
CREATE TABLE "public"."core_wwusadvantage" (
"id" int4 DEFAULT nextval('core_wwusadvantage_id_seq'::regclass) NOT NULL,
"name" varchar(200) COLLATE "default" NOT NULL,
"description" text COLLATE "default" NOT NULL,
"image" varchar(100) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of core_wwusadvantage
-- ----------------------------
INSERT INTO "public"."core_wwusadvantage" VALUES ('1', 'Less Cadro', 'By using our services you can reduce your employers.By using our services you can reduce your employers.By using our services you can reduce your employers.By using our services you can reduce your employers.By using our services you can reduce your employers.By using our services you can reduce your employers.By using our services you can reduce your employers.By using our services you can reduce your employers.By using our services you can reduce your employers.', 'uploads/wwusadvantage/chartLine.png');
INSERT INTO "public"."core_wwusadvantage" VALUES ('2', 'Cheap Services', 'The services we offer are cheaper than equivalents.The services we offer are cheaper than equivalents.The services we offer are cheaper than equivalents.The services we offer are cheaper than equivalents.The services we offer are cheaper than equivalents.The services we offer are cheaper than equivalents.', 'uploads/wwusadvantage/chartPie.png');
INSERT INTO "public"."core_wwusadvantage" VALUES ('3', 'Faster Solutions', 'If you take some services from Bsss co. you can solve your problems faster than ever.If you take some services from Bsss co. you can solve your problems faster than ever.If you take some services from Bsss co. you can solve your problems faster than ever.If you take some services from Bsss co. you can solve your problems faster than ever.If you take some services from Bsss co. you can solve your problems faster than ever.If you take some services from Bsss co. you can solve your problems faster than ever.', 'uploads/wwusadvantage/chartBlock.png');

-- ----------------------------
-- Table structure for django_admin_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."django_admin_log";
CREATE TABLE "public"."django_admin_log" (
"id" int4 DEFAULT nextval('django_admin_log_id_seq'::regclass) NOT NULL,
"action_time" timestamptz(6) NOT NULL,
"object_id" text COLLATE "default",
"object_repr" varchar(200) COLLATE "default" NOT NULL,
"action_flag" int2 NOT NULL,
"change_message" text COLLATE "default" NOT NULL,
"content_type_id" int4,
"user_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of django_admin_log
-- ----------------------------
INSERT INTO "public"."django_admin_log" VALUES ('1', '2017-04-14 04:06:13.063965-07', '1', 'Service object', '1', '[{"added": {}}]', '7', '1');
INSERT INTO "public"."django_admin_log" VALUES ('2', '2017-04-14 04:06:56.567453-07', '2', 'Service object', '1', '[{"added": {}}]', '7', '1');
INSERT INTO "public"."django_admin_log" VALUES ('3', '2017-04-17 05:53:29.871572-07', '1', 'Package object', '1', '[{"added": {}}]', '8', '1');
INSERT INTO "public"."django_admin_log" VALUES ('4', '2017-04-17 05:53:54.256967-07', '2', 'Package object', '1', '[{"added": {}}]', '8', '1');
INSERT INTO "public"."django_admin_log" VALUES ('5', '2017-04-19 04:30:06.902522-07', '1', 'CompanyCategory object', '1', '[{"added": {}}]', '10', '1');
INSERT INTO "public"."django_admin_log" VALUES ('6', '2017-04-19 04:30:34.187082-07', '2', 'CompanyCategory object', '1', '[{"added": {}}]', '10', '1');
INSERT INTO "public"."django_admin_log" VALUES ('7', '2017-04-19 04:31:25.263004-07', '3', 'CompanyCategory object', '1', '[{"added": {}}]', '10', '1');
INSERT INTO "public"."django_admin_log" VALUES ('8', '2017-04-19 05:07:30.845868-07', '1', 'Company object', '1', '[{"added": {}}]', '12', '1');
INSERT INTO "public"."django_admin_log" VALUES ('9', '2017-04-19 05:09:22.839273-07', '1', 'PackageCategory object', '1', '[{"added": {}}]', '13', '1');
INSERT INTO "public"."django_admin_log" VALUES ('10', '2017-04-19 05:10:00.235412-07', '1', 'PackageCategory object', '2', '[{"changed": {"fields": ["name"]}}]', '13', '1');
INSERT INTO "public"."django_admin_log" VALUES ('11', '2017-04-19 05:10:21.429625-07', '1', 'PackageRegion object', '1', '[{"added": {}}]', '16', '1');
INSERT INTO "public"."django_admin_log" VALUES ('12', '2017-04-19 05:12:06.993663-07', '1', 'PackageType object', '1', '[{"added": {}}]', '15', '1');
INSERT INTO "public"."django_admin_log" VALUES ('13', '2017-04-19 05:13:18.867774-07', '1', 'Package object', '1', '[{"added": {}}]', '11', '1');
INSERT INTO "public"."django_admin_log" VALUES ('14', '2017-04-19 05:13:51.391634-07', '1', 'Services object', '1', '[{"added": {}}]', '14', '1');
INSERT INTO "public"."django_admin_log" VALUES ('15', '2017-04-19 05:59:32.880438-07', '2', 'PackageType object', '1', '[{"added": {}}]', '15', '1');
INSERT INTO "public"."django_admin_log" VALUES ('16', '2017-04-19 06:30:05.270245-07', '2', 'Company object', '1', '[{"added": {}}]', '12', '1');
INSERT INTO "public"."django_admin_log" VALUES ('17', '2017-04-19 07:33:44.599698-07', '2', 'PackageCategory object', '1', '[{"added": {}}]', '13', '1');
INSERT INTO "public"."django_admin_log" VALUES ('18', '2017-04-20 04:16:51.647547-07', '2', 'Services object', '1', '[{"added": {}}]', '14', '1');
INSERT INTO "public"."django_admin_log" VALUES ('19', '2017-04-20 05:12:10.809393-07', '3', 'Company object', '1', '[{"added": {}}]', '12', '1');
INSERT INTO "public"."django_admin_log" VALUES ('20', '2017-04-20 05:23:33.808458-07', '3', 'CompanyCategory object', '2', '[{"changed": {"fields": ["image"]}}]', '10', '1');
INSERT INTO "public"."django_admin_log" VALUES ('21', '2017-04-20 05:32:02.587558-07', '3', 'CompanyCategory object', '2', '[{"changed": {"fields": ["image"]}}]', '10', '1');
INSERT INTO "public"."django_admin_log" VALUES ('22', '2017-04-20 06:36:07.975502-07', '3', 'CompanyCategory object', '2', '[]', '10', '1');
INSERT INTO "public"."django_admin_log" VALUES ('23', '2017-04-20 06:36:45.17663-07', '4', 'CompanyCategory object', '1', '[{"added": {}}]', '10', '1');
INSERT INTO "public"."django_admin_log" VALUES ('24', '2017-04-21 04:15:45.826542-07', '4', 'Company object', '1', '[{"added": {}}]', '12', '1');
INSERT INTO "public"."django_admin_log" VALUES ('25', '2017-04-21 04:16:51.477236-07', '5', 'Company object', '1', '[{"added": {}}]', '12', '1');
INSERT INTO "public"."django_admin_log" VALUES ('26', '2017-04-21 04:19:06.768975-07', '2', 'Package object', '1', '[{"added": {}}]', '11', '1');
INSERT INTO "public"."django_admin_log" VALUES ('27', '2017-04-21 04:21:04.691717-07', '3', 'Package object', '1', '[{"added": {}}]', '11', '1');
INSERT INTO "public"."django_admin_log" VALUES ('28', '2017-04-21 04:21:46.517109-07', '4', 'Package object', '1', '[{"added": {}}]', '11', '1');
INSERT INTO "public"."django_admin_log" VALUES ('29', '2017-04-21 04:22:06.304241-07', '5', 'Package object', '1', '[{"added": {}}]', '11', '1');
INSERT INTO "public"."django_admin_log" VALUES ('30', '2017-04-21 04:22:30.027598-07', '6', 'Package object', '1', '[{"added": {}}]', '11', '1');
INSERT INTO "public"."django_admin_log" VALUES ('31', '2017-04-21 04:27:48.939168-07', '3', 'Services object', '1', '[{"added": {}}]', '14', '1');
INSERT INTO "public"."django_admin_log" VALUES ('32', '2017-04-21 04:28:02.704956-07', '4', 'Services object', '1', '[{"added": {}}]', '14', '1');
INSERT INTO "public"."django_admin_log" VALUES ('33', '2017-04-21 04:28:20.905997-07', '5', 'Services object', '1', '[{"added": {}}]', '14', '1');
INSERT INTO "public"."django_admin_log" VALUES ('34', '2017-04-21 04:28:34.315764-07', '6', 'Services object', '1', '[{"added": {}}]', '14', '1');
INSERT INTO "public"."django_admin_log" VALUES ('35', '2017-04-21 04:28:48.350566-07', '7', 'Services object', '1', '[{"added": {}}]', '14', '1');
INSERT INTO "public"."django_admin_log" VALUES ('36', '2017-04-21 04:29:28.650871-07', '8', 'Services object', '1', '[{"added": {}}]', '14', '1');
INSERT INTO "public"."django_admin_log" VALUES ('37', '2017-04-21 04:29:40.776565-07', '9', 'Services object', '1', '[{"added": {}}]', '14', '1');
INSERT INTO "public"."django_admin_log" VALUES ('38', '2017-04-21 04:29:50.574125-07', '10', 'Services object', '1', '[{"added": {}}]', '14', '1');
INSERT INTO "public"."django_admin_log" VALUES ('39', '2017-04-21 04:30:01.778766-07', '11', 'Services object', '1', '[{"added": {}}]', '14', '1');
INSERT INTO "public"."django_admin_log" VALUES ('40', '2017-04-21 04:30:12.106357-07', '12', 'Services object', '1', '[{"added": {}}]', '14', '1');
INSERT INTO "public"."django_admin_log" VALUES ('41', '2017-04-21 04:30:18.738736-07', '13', 'Services object', '1', '[{"added": {}}]', '14', '1');
INSERT INTO "public"."django_admin_log" VALUES ('42', '2017-04-21 04:30:31.158447-07', '14', 'Services object', '1', '[{"added": {}}]', '14', '1');
INSERT INTO "public"."django_admin_log" VALUES ('43', '2017-05-01 06:03:31.938526-07', '1', 'Menu object', '1', '[{"added": {}}]', '20', '1');
INSERT INTO "public"."django_admin_log" VALUES ('44', '2017-05-01 06:04:17.933244-07', '2', 'Menu object', '1', '[{"added": {}}]', '20', '1');
INSERT INTO "public"."django_admin_log" VALUES ('45', '2017-05-01 06:04:51.931843-07', '3', 'Menu object', '1', '[{"added": {}}]', '20', '1');
INSERT INTO "public"."django_admin_log" VALUES ('46', '2017-05-01 06:05:12.52775-07', '3', 'Menu object', '2', '[{"changed": {"fields": ["name", "url", "number"]}}]', '20', '1');
INSERT INTO "public"."django_admin_log" VALUES ('47', '2017-05-01 06:05:48.820309-07', '3', 'Menu object', '2', '[{"changed": {"fields": ["name", "url", "number"]}}]', '20', '1');
INSERT INTO "public"."django_admin_log" VALUES ('48', '2017-05-01 06:06:04.461542-07', '3', 'Menu object', '2', '[{"changed": {"fields": ["name", "url", "number"]}}]', '20', '1');
INSERT INTO "public"."django_admin_log" VALUES ('49', '2017-05-01 06:06:18.097495-07', '3', 'Menu object', '2', '[{"changed": {"fields": ["name", "url", "number"]}}]', '20', '1');
INSERT INTO "public"."django_admin_log" VALUES ('50', '2017-05-01 06:10:04.257553-07', '4', 'Menu object', '1', '[{"added": {}}]', '20', '1');
INSERT INTO "public"."django_admin_log" VALUES ('51', '2017-05-01 06:10:37.320328-07', '5', 'Menu object', '1', '[{"added": {}}]', '20', '1');
INSERT INTO "public"."django_admin_log" VALUES ('52', '2017-05-01 06:11:05.138371-07', '5', 'Menu object', '2', '[{"changed": {"fields": ["name", "url", "number"]}}]', '20', '1');
INSERT INTO "public"."django_admin_log" VALUES ('53', '2017-05-01 06:11:39.164591-07', '5', 'Menu object', '2', '[{"changed": {"fields": ["name", "url", "number"]}}]', '20', '1');
INSERT INTO "public"."django_admin_log" VALUES ('54', '2017-05-01 06:16:25.454968-07', '6', 'Menu object', '2', '[{"changed": {"fields": ["name", "url", "number"]}}]', '20', '1');
INSERT INTO "public"."django_admin_log" VALUES ('55', '2017-05-01 06:19:12.878544-07', '7', 'Menu object', '1', '[{"added": {}}]', '20', '1');
INSERT INTO "public"."django_admin_log" VALUES ('56', '2017-05-01 06:20:02.263369-07', '3', 'Menu object', '2', '[{"changed": {"fields": ["number"]}}]', '20', '1');
INSERT INTO "public"."django_admin_log" VALUES ('57', '2017-05-01 06:22:36.032164-07', '1', 'Slide object', '1', '[{"added": {}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('58', '2017-05-01 06:24:06.566342-07', '2', 'Slide object', '1', '[{"added": {}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('59', '2017-05-01 07:33:58.519108-07', '1', 'BsssImagination object', '1', '[{"added": {}}]', '17', '1');
INSERT INTO "public"."django_admin_log" VALUES ('60', '2017-05-02 05:44:22.771228-07', '3', 'Slide object', '1', '[{"added": {}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('61', '2017-05-02 05:47:07.436933-07', '3', 'Slide object', '3', '', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('62', '2017-05-02 05:51:33.035234-07', '4', 'Slide object', '1', '[{"added": {}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('63', '2017-05-02 06:08:09.546679-07', '5', 'Slide object', '1', '[{"added": {}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('64', '2017-05-02 06:08:36.308377-07', '5', 'Slide object', '3', '', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('65', '2017-05-02 06:22:36.01932-07', '6', 'Slide object', '1', '[{"added": {}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('66', '2017-05-03 06:11:43.9834-07', '6', 'Slide object', '3', '', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('67', '2017-05-03 06:12:06.030278-07', '7', 'Slide object', '1', '[{"added": {}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('68', '2017-05-03 06:14:45.542065-07', '8', 'Slide object', '1', '[{"added": {}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('69', '2017-05-03 06:17:45.070759-07', '8', 'Slide object', '3', '', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('70', '2017-05-03 06:17:45.118762-07', '7', 'Slide object', '3', '', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('71', '2017-05-03 06:18:02.359982-07', '9', 'Slide object', '1', '[{"added": {}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('72', '2017-05-03 06:22:55.540616-07', '10', 'Slide object', '1', '[{"added": {}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('73', '2017-05-03 06:27:27.061346-07', '11', 'Slide object', '1', '[{"added": {}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('74', '2017-05-03 06:28:40.204452-07', '12', 'Slide object', '1', '[{"added": {}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('75', '2017-05-03 06:29:50.70949-07', '13', 'Slide object', '1', '[{"added": {}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('76', '2017-05-03 06:31:52.930528-07', '14', 'Slide object', '1', '[{"added": {}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('77', '2017-05-03 06:33:13.598532-07', '14', 'Slide object', '3', '', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('78', '2017-05-03 06:33:13.723539-07', '13', 'Slide object', '3', '', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('79', '2017-05-03 06:33:13.726539-07', '12', 'Slide object', '3', '', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('80', '2017-05-03 06:33:13.728539-07', '11', 'Slide object', '3', '', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('81', '2017-05-03 06:33:13.731539-07', '10', 'Slide object', '3', '', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('82', '2017-05-03 06:33:13.734539-07', '9', 'Slide object', '3', '', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('83', '2017-05-03 06:33:26.578581-07', '15', 'Slide object', '1', '[{"added": {}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('84', '2017-05-03 06:35:19.780136-07', '16', 'Slide object', '1', '[{"added": {}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('85', '2017-05-03 06:37:29.337965-07', '16', 'Slide object', '3', '', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('86', '2017-05-03 06:37:29.568978-07', '15', 'Slide object', '3', '', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('87', '2017-05-03 06:37:54.915747-07', '17', 'Slide object', '1', '[{"added": {}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('88', '2017-05-03 06:48:32.640779-07', '18', 'Slide object', '1', '[{"added": {}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('89', '2017-05-03 06:54:01.254556-07', '1', 'Slide object', '2', '[{"changed": {"fields": ["image"]}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('90', '2017-05-03 06:54:20.891679-07', '2', 'Slide object', '2', '[{"changed": {"fields": ["image"]}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('91', '2017-05-03 06:54:30.125207-07', '4', 'Slide object', '2', '[{"changed": {"fields": ["image"]}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('92', '2017-05-03 06:54:43.64346-07', '18', 'Slide object', '3', '', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('93', '2017-05-03 06:54:43.694463-07', '17', 'Slide object', '3', '', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('94', '2017-05-03 06:56:50.26812-07', '1', 'Slide object', '2', '[{"changed": {"fields": ["image"]}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('95', '2017-05-03 06:57:40.06815-07', '1', 'Slide object', '2', '[{"changed": {"fields": ["image"]}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('96', '2017-05-03 06:58:07.734488-07', '2', 'Slide object', '2', '[{"changed": {"fields": ["image"]}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('97', '2017-05-03 06:58:15.401234-07', '4', 'Slide object', '2', '[{"changed": {"fields": ["image"]}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('98', '2017-05-03 06:59:51.01195-07', '1', 'BsssImagination object', '2', '[{"changed": {"fields": ["image"]}}]', '17', '1');
INSERT INTO "public"."django_admin_log" VALUES ('99', '2017-05-03 07:20:10.145824-07', '6', 'Menu object', '2', '[{"changed": {"fields": ["name"]}}]', '20', '1');
INSERT INTO "public"."django_admin_log" VALUES ('100', '2017-05-03 07:20:37.252191-07', '4', 'Slide object', '2', '[{"changed": {"fields": ["image"]}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('101', '2017-05-03 07:21:09.165433-07', '4', 'Slide object', '2', '[{"changed": {"fields": ["image"]}}]', '21', '1');
INSERT INTO "public"."django_admin_log" VALUES ('102', '2017-05-03 07:51:21.399085-07', '1', 'CompanyCategory object', '1', '[{"added": {}}]', '10', '1');
INSERT INTO "public"."django_admin_log" VALUES ('103', '2017-05-03 07:51:34.745848-07', '2', 'CompanyCategory object', '1', '[{"added": {}}]', '10', '1');
INSERT INTO "public"."django_admin_log" VALUES ('104', '2017-05-03 07:52:49.0871-07', '1', 'CompanyType object', '1', '[{"added": {}}]', '23', '1');
INSERT INTO "public"."django_admin_log" VALUES ('105', '2017-05-03 07:53:20.949923-07', '2', 'CompanyType object', '1', '[{"added": {}}]', '23', '1');
INSERT INTO "public"."django_admin_log" VALUES ('106', '2017-05-03 07:54:05.291459-07', '3', 'CompanyType object', '1', '[{"added": {}}]', '23', '1');
INSERT INTO "public"."django_admin_log" VALUES ('107', '2017-05-03 08:18:42.199933-07', '2', 'CompanyCategory object', '2', '[{"changed": {"fields": ["image"]}}]', '10', '1');
INSERT INTO "public"."django_admin_log" VALUES ('108', '2017-05-03 08:19:10.895574-07', '1', 'CompanyCategory object', '2', '[{"changed": {"fields": ["image"]}}]', '10', '1');
INSERT INTO "public"."django_admin_log" VALUES ('109', '2017-05-03 08:21:44.860381-07', '6', 'Menu object', '2', '[{"changed": {"fields": ["name"]}}]', '20', '1');
INSERT INTO "public"."django_admin_log" VALUES ('110', '2017-05-03 08:24:27.362675-07', '1', 'HowDoesItWork object', '1', '[{"added": {}}]', '19', '1');
INSERT INTO "public"."django_admin_log" VALUES ('111', '2017-05-03 08:34:39.303676-07', '1', 'HowDoesItWork object', '2', '[{"changed": {"fields": ["presentation"]}}]', '19', '1');
INSERT INTO "public"."django_admin_log" VALUES ('112', '2017-05-03 08:37:09.36926-07', '1', 'HowDoesItWork object', '2', '[{"changed": {"fields": ["presentation"]}}]', '19', '1');
INSERT INTO "public"."django_admin_log" VALUES ('113', '2017-05-04 04:58:55.521966-07', '1', 'WWUsAdvantage object', '1', '[{"added": {}}]', '22', '1');
INSERT INTO "public"."django_admin_log" VALUES ('114', '2017-05-04 05:01:20.419381-07', '2', 'WWUsAdvantage object', '1', '[{"added": {}}]', '22', '1');
INSERT INTO "public"."django_admin_log" VALUES ('115', '2017-05-04 05:02:46.687372-07', '3', 'WWUsAdvantage object', '1', '[{"added": {}}]', '22', '1');
INSERT INTO "public"."django_admin_log" VALUES ('116', '2017-05-04 05:02:53.879566-07', '2', 'WWUsAdvantage object', '2', '[{"changed": {"fields": ["description"]}}]', '22', '1');
INSERT INTO "public"."django_admin_log" VALUES ('117', '2017-05-04 05:02:59.742738-07', '1', 'WWUsAdvantage object', '2', '[{"changed": {"fields": ["description"]}}]', '22', '1');
INSERT INTO "public"."django_admin_log" VALUES ('118', '2017-05-04 05:14:13.190089-07', '1', 'Portfolio object', '1', '[{"added": {}}]', '27', '1');
INSERT INTO "public"."django_admin_log" VALUES ('119', '2017-05-04 05:16:46.777361-07', '1', 'Portfolio object', '3', '', '27', '1');
INSERT INTO "public"."django_admin_log" VALUES ('120', '2017-05-04 05:18:54.946801-07', '2', 'Portfolio object', '1', '[{"added": {}}]', '27', '1');
INSERT INTO "public"."django_admin_log" VALUES ('121', '2017-05-04 05:19:36.579472-07', '3', 'Portfolio object', '1', '[{"added": {}}]', '27', '1');
INSERT INTO "public"."django_admin_log" VALUES ('122', '2017-05-04 05:20:02.321758-07', '4', 'Portfolio object', '1', '[{"added": {}}]', '27', '1');
INSERT INTO "public"."django_admin_log" VALUES ('123', '2017-05-04 05:21:06.989222-07', '5', 'Portfolio object', '1', '[{"added": {}}]', '27', '1');
INSERT INTO "public"."django_admin_log" VALUES ('124', '2017-05-04 05:47:44.268524-07', '2', 'Portfolio object', '2', '[{"changed": {"fields": ["description"]}}]', '27', '1');
INSERT INTO "public"."django_admin_log" VALUES ('125', '2017-05-04 05:47:58.099869-07', '3', 'Portfolio object', '2', '[{"changed": {"fields": ["description"]}}]', '27', '1');
INSERT INTO "public"."django_admin_log" VALUES ('126', '2017-05-04 05:48:06.572078-07', '4', 'Portfolio object', '2', '[{"changed": {"fields": ["description"]}}]', '27', '1');
INSERT INTO "public"."django_admin_log" VALUES ('127', '2017-05-04 05:48:13.594263-07', '5', 'Portfolio object', '2', '[{"changed": {"fields": ["description"]}}]', '27', '1');
INSERT INTO "public"."django_admin_log" VALUES ('128', '2017-05-04 06:38:56.73273-07', '2', 'Portfolio object', '2', '[{"changed": {"fields": ["name"]}}]', '27', '1');
INSERT INTO "public"."django_admin_log" VALUES ('129', '2017-05-04 07:38:03.555843-07', '1', 'PortfolioImages object', '1', '[{"added": {}}]', '28', '1');
INSERT INTO "public"."django_admin_log" VALUES ('130', '2017-05-04 07:38:35.473669-07', '2', 'PortfolioImages object', '1', '[{"added": {}}]', '28', '1');
INSERT INTO "public"."django_admin_log" VALUES ('131', '2017-05-04 07:41:01.66703-07', '3', 'PortfolioImages object', '1', '[{"added": {}}]', '28', '1');
INSERT INTO "public"."django_admin_log" VALUES ('132', '2017-05-05 02:00:47.363098-07', '1', 'Partner object', '1', '[{"added": {}}]', '29', '1');
INSERT INTO "public"."django_admin_log" VALUES ('133', '2017-05-05 02:01:32.254665-07', '2', 'Partner object', '1', '[{"added": {}}]', '29', '1');
INSERT INTO "public"."django_admin_log" VALUES ('134', '2017-05-05 04:10:20.829358-07', '2', 'CompanyType object', '2', '[{"changed": {"fields": ["slug"]}}]', '23', '1');
INSERT INTO "public"."django_admin_log" VALUES ('135', '2017-05-05 04:10:48.092917-07', '1', 'CompanyType object', '2', '[{"changed": {"fields": ["slug"]}}]', '23', '1');
INSERT INTO "public"."django_admin_log" VALUES ('136', '2017-05-05 04:11:00.018599-07', '3', 'CompanyType object', '2', '[{"changed": {"fields": ["slug"]}}]', '23', '1');
INSERT INTO "public"."django_admin_log" VALUES ('137', '2017-05-05 04:36:39.731666-07', '1', 'CompanyType object', '2', '[{"changed": {"fields": ["presentation"]}}]', '23', '1');
INSERT INTO "public"."django_admin_log" VALUES ('138', '2017-05-05 04:36:46.835072-07', '2', 'CompanyType object', '2', '[{"changed": {"fields": ["presentation"]}}]', '23', '1');
INSERT INTO "public"."django_admin_log" VALUES ('139', '2017-05-05 04:37:01.226895-07', '3', 'CompanyType object', '2', '[{"changed": {"fields": ["presentation"]}}]', '23', '1');
INSERT INTO "public"."django_admin_log" VALUES ('140', '2017-05-05 04:45:12.678005-07', '1', 'Support object', '1', '[{"added": {}}]', '25', '1');
INSERT INTO "public"."django_admin_log" VALUES ('141', '2017-05-05 04:54:58.841531-07', '2', 'Support object', '1', '[{"added": {}}]', '25', '1');
INSERT INTO "public"."django_admin_log" VALUES ('142', '2017-05-05 04:56:47.465744-07', '1', 'Support object', '2', '[{"changed": {"fields": ["name", "description"]}}]', '25', '1');
INSERT INTO "public"."django_admin_log" VALUES ('143', '2017-05-05 04:56:59.473431-07', '1', 'Support object', '2', '[{"changed": {"fields": ["slug"]}}]', '25', '1');
INSERT INTO "public"."django_admin_log" VALUES ('144', '2017-05-05 04:57:56.987721-07', '3', 'Support object', '1', '[{"added": {}}]', '25', '1');
INSERT INTO "public"."django_admin_log" VALUES ('145', '2017-05-05 04:59:02.00644-07', '1', 'Support object', '2', '[{"changed": {"fields": ["image"]}}]', '25', '1');
INSERT INTO "public"."django_admin_log" VALUES ('146', '2017-05-05 05:44:00.145764-07', '4', 'Support object', '1', '[{"added": {}}]', '25', '1');
INSERT INTO "public"."django_admin_log" VALUES ('147', '2017-05-05 05:45:05.691513-07', '5', 'Support object', '1', '[{"added": {}}]', '25', '1');
INSERT INTO "public"."django_admin_log" VALUES ('148', '2017-05-05 05:46:32.197461-07', '6', 'Support object', '1', '[{"added": {}}]', '25', '1');
INSERT INTO "public"."django_admin_log" VALUES ('149', '2017-05-05 05:47:57.775356-07', '7', 'Support object', '1', '[{"added": {}}]', '25', '1');
INSERT INTO "public"."django_admin_log" VALUES ('150', '2017-05-05 05:56:02.472079-07', '1', 'Service object', '1', '[{"added": {}}]', '24', '1');
INSERT INTO "public"."django_admin_log" VALUES ('151', '2017-05-05 05:59:18.514292-07', '2', 'Service object', '1', '[{"added": {}}]', '24', '1');
INSERT INTO "public"."django_admin_log" VALUES ('152', '2017-05-05 05:59:37.971405-07', '3', 'Support object', '2', '[{"changed": {"fields": ["name"]}}]', '25', '1');
INSERT INTO "public"."django_admin_log" VALUES ('153', '2017-05-05 06:02:10.241114-07', '3', 'Service object', '1', '[{"added": {}}]', '24', '1');
INSERT INTO "public"."django_admin_log" VALUES ('154', '2017-05-05 06:03:32.108797-07', '4', 'Service object', '1', '[{"added": {}}]', '24', '1');
INSERT INTO "public"."django_admin_log" VALUES ('155', '2017-05-05 06:04:19.354499-07', '5', 'Service object', '1', '[{"added": {}}]', '24', '1');
INSERT INTO "public"."django_admin_log" VALUES ('156', '2017-05-05 06:05:59.877249-07', '6', 'Service object', '1', '[{"added": {}}]', '24', '1');
INSERT INTO "public"."django_admin_log" VALUES ('157', '2017-05-05 06:18:32.616303-07', '7', 'Service object', '1', '[{"added": {}}]', '24', '1');
INSERT INTO "public"."django_admin_log" VALUES ('158', '2017-05-05 06:19:45.598477-07', '8', 'Service object', '1', '[{"added": {}}]', '24', '1');
INSERT INTO "public"."django_admin_log" VALUES ('159', '2017-05-05 07:17:06.712298-07', '3', 'Support object', '2', '[{"changed": {"fields": ["slug"]}}]', '25', '1');
INSERT INTO "public"."django_admin_log" VALUES ('160', '2017-05-05 23:12:39.433855-07', '3', 'CompanyType object', '2', '[{"changed": {"fields": ["description"]}}]', '23', '1');
INSERT INTO "public"."django_admin_log" VALUES ('161', '2017-05-05 23:33:37.687791-07', '3', 'CompanyType object', '2', '[{"changed": {"fields": ["description"]}}]', '23', '1');
INSERT INTO "public"."django_admin_log" VALUES ('162', '2017-05-05 23:40:09.083361-07', '3', 'CompanyType object', '2', '[{"changed": {"fields": ["description"]}}]', '23', '1');
INSERT INTO "public"."django_admin_log" VALUES ('163', '2017-05-05 23:40:44.590582-07', '3', 'CompanyType object', '2', '[{"changed": {"fields": ["description"]}}]', '23', '1');
INSERT INTO "public"."django_admin_log" VALUES ('164', '2017-05-05 23:41:05.752747-07', '3', 'CompanyType object', '2', '[{"changed": {"fields": ["description"]}}]', '23', '1');
INSERT INTO "public"."django_admin_log" VALUES ('165', '2017-05-06 00:19:08.375441-07', '1', 'SupportAdvantage object', '1', '[{"added": {}}]', '26', '1');
INSERT INTO "public"."django_admin_log" VALUES ('166', '2017-05-06 00:20:22.264882-07', '2', 'SupportAdvantage object', '1', '[{"added": {}}]', '26', '1');
INSERT INTO "public"."django_admin_log" VALUES ('167', '2017-05-08 08:04:21.945391-07', '3', 'Menu object', '3', '', '20', '1');
INSERT INTO "public"."django_admin_log" VALUES ('168', '2017-05-09 14:42:55.360258-07', '1', 'SaleRecommendation object', '1', '[{"added": {}}]', '30', '1');
INSERT INTO "public"."django_admin_log" VALUES ('169', '2017-05-09 15:06:11.433109-07', '2', 'SaleRecommendation object', '1', '[{"added": {}}]', '30', '1');
INSERT INTO "public"."django_admin_log" VALUES ('170', '2017-05-09 15:21:19.55505-07', '1', 'ProjectRecommendation object', '1', '[{"added": {}}]', '31', '1');
INSERT INTO "public"."django_admin_log" VALUES ('171', '2017-05-09 15:21:41.432302-07', '2', 'ProjectRecommendation object', '1', '[{"added": {}}]', '31', '1');
INSERT INTO "public"."django_admin_log" VALUES ('172', '2017-05-10 03:38:56.833333-07', '3', 'ProjectRecommendation object', '1', '[{"added": {}}]', '31', '1');
INSERT INTO "public"."django_admin_log" VALUES ('173', '2017-05-10 03:39:19.449626-07', '4', 'ProjectRecommendation object', '1', '[{"added": {}}]', '31', '1');
INSERT INTO "public"."django_admin_log" VALUES ('174', '2017-05-10 03:39:50.675412-07', '5', 'ProjectRecommendation object', '1', '[{"added": {}}]', '31', '1');
INSERT INTO "public"."django_admin_log" VALUES ('175', '2017-05-10 03:59:41.179505-07', '6', 'ProjectRecommendation object', '1', '[{"added": {}}]', '31', '1');
INSERT INTO "public"."django_admin_log" VALUES ('176', '2017-05-10 05:13:33.647028-07', '7', 'ProjectRecommendation object', '1', '[{"added": {}}]', '31', '1');
INSERT INTO "public"."django_admin_log" VALUES ('177', '2017-05-10 05:13:33.884042-07', '8', 'ProjectRecommendation object', '1', '[{"added": {}}]', '31', '1');
INSERT INTO "public"."django_admin_log" VALUES ('178', '2017-05-10 07:22:54.105901-07', '9', 'ProjectRecommendation object', '1', '[{"added": {}}]', '31', '1');
INSERT INTO "public"."django_admin_log" VALUES ('179', '2017-05-10 07:23:42.626676-07', '10', 'ProjectRecommendation object', '1', '[{"added": {}}]', '31', '1');
INSERT INTO "public"."django_admin_log" VALUES ('180', '2017-05-11 04:21:41.440043-07', '11', 'ProjectRecommendation object', '1', '[{"added": {}}]', '31', '1');

-- ----------------------------
-- Table structure for django_content_type
-- ----------------------------
DROP TABLE IF EXISTS "public"."django_content_type";
CREATE TABLE "public"."django_content_type" (
"id" int4 DEFAULT nextval('django_content_type_id_seq'::regclass) NOT NULL,
"app_label" varchar(100) COLLATE "default" NOT NULL,
"model" varchar(100) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of django_content_type
-- ----------------------------
INSERT INTO "public"."django_content_type" VALUES ('1', 'admin', 'logentry');
INSERT INTO "public"."django_content_type" VALUES ('2', 'auth', 'permission');
INSERT INTO "public"."django_content_type" VALUES ('3', 'auth', 'group');
INSERT INTO "public"."django_content_type" VALUES ('4', 'auth', 'user');
INSERT INTO "public"."django_content_type" VALUES ('5', 'contenttypes', 'contenttype');
INSERT INTO "public"."django_content_type" VALUES ('6', 'sessions', 'session');
INSERT INTO "public"."django_content_type" VALUES ('7', 'services', 'service');
INSERT INTO "public"."django_content_type" VALUES ('8', 'Packages', 'package');
INSERT INTO "public"."django_content_type" VALUES ('9', 'Packages', 'packageline');
INSERT INTO "public"."django_content_type" VALUES ('10', 'Services', 'companycategory');
INSERT INTO "public"."django_content_type" VALUES ('11', 'Services', 'package');
INSERT INTO "public"."django_content_type" VALUES ('12', 'Services', 'company');
INSERT INTO "public"."django_content_type" VALUES ('13', 'Services', 'packagecategory');
INSERT INTO "public"."django_content_type" VALUES ('14', 'Services', 'services');
INSERT INTO "public"."django_content_type" VALUES ('15', 'Services', 'packagetype');
INSERT INTO "public"."django_content_type" VALUES ('16', 'Services', 'packageregion');
INSERT INTO "public"."django_content_type" VALUES ('17', 'core', 'bsssimagination');
INSERT INTO "public"."django_content_type" VALUES ('18', 'core', 'contactus');
INSERT INTO "public"."django_content_type" VALUES ('19', 'core', 'howdoesitwork');
INSERT INTO "public"."django_content_type" VALUES ('20', 'core', 'menu');
INSERT INTO "public"."django_content_type" VALUES ('21', 'core', 'slide');
INSERT INTO "public"."django_content_type" VALUES ('22', 'core', 'wwusadvantage');
INSERT INTO "public"."django_content_type" VALUES ('23', 'Services', 'companytype');
INSERT INTO "public"."django_content_type" VALUES ('24', 'Services', 'service');
INSERT INTO "public"."django_content_type" VALUES ('25', 'Services', 'support');
INSERT INTO "public"."django_content_type" VALUES ('26', 'Services', 'supportadvantage');
INSERT INTO "public"."django_content_type" VALUES ('27', 'core', 'portfolio');
INSERT INTO "public"."django_content_type" VALUES ('28', 'core', 'portfolioimages');
INSERT INTO "public"."django_content_type" VALUES ('29', 'core', 'partner');
INSERT INTO "public"."django_content_type" VALUES ('30', 'Auth', 'salerecommendation');
INSERT INTO "public"."django_content_type" VALUES ('31', 'Auth', 'projectrecommendation');

-- ----------------------------
-- Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS "public"."django_migrations";
CREATE TABLE "public"."django_migrations" (
"id" int4 DEFAULT nextval('django_migrations_id_seq'::regclass) NOT NULL,
"app" varchar(255) COLLATE "default" NOT NULL,
"name" varchar(255) COLLATE "default" NOT NULL,
"applied" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of django_migrations
-- ----------------------------
INSERT INTO "public"."django_migrations" VALUES ('1', 'contenttypes', '0001_initial', '2017-04-13 07:13:59.713352-07');
INSERT INTO "public"."django_migrations" VALUES ('2', 'auth', '0001_initial', '2017-04-13 07:14:01.211438-07');
INSERT INTO "public"."django_migrations" VALUES ('3', 'admin', '0001_initial', '2017-04-13 07:14:01.60746-07');
INSERT INTO "public"."django_migrations" VALUES ('4', 'admin', '0002_logentry_remove_auto_add', '2017-04-13 07:14:01.633462-07');
INSERT INTO "public"."django_migrations" VALUES ('5', 'contenttypes', '0002_remove_content_type_name', '2017-04-13 07:14:01.709466-07');
INSERT INTO "public"."django_migrations" VALUES ('6', 'auth', '0002_alter_permission_name_max_length', '2017-04-13 07:14:01.725467-07');
INSERT INTO "public"."django_migrations" VALUES ('7', 'auth', '0003_alter_user_email_max_length', '2017-04-13 07:14:01.752469-07');
INSERT INTO "public"."django_migrations" VALUES ('8', 'auth', '0004_alter_user_username_opts', '2017-04-13 07:14:01.77647-07');
INSERT INTO "public"."django_migrations" VALUES ('9', 'auth', '0005_alter_user_last_login_null', '2017-04-13 07:14:01.803471-07');
INSERT INTO "public"."django_migrations" VALUES ('10', 'auth', '0006_require_contenttypes_0002', '2017-04-13 07:14:01.809472-07');
INSERT INTO "public"."django_migrations" VALUES ('11', 'auth', '0007_alter_validators_add_error_messages', '2017-04-13 07:14:01.834473-07');
INSERT INTO "public"."django_migrations" VALUES ('12', 'auth', '0008_alter_user_username_max_length', '2017-04-13 07:14:01.94748-07');
INSERT INTO "public"."django_migrations" VALUES ('13', 'sessions', '0001_initial', '2017-04-13 07:14:02.13449-07');
INSERT INTO "public"."django_migrations" VALUES ('14', 'services', '0001_initial', '2017-04-13 07:37:07.306718-07');
INSERT INTO "public"."django_migrations" VALUES ('15', 'Packages', '0001_initial', '2017-04-17 05:43:53.381583-07');
INSERT INTO "public"."django_migrations" VALUES ('16', 'Services', '0001_initial', '2017-04-18 07:37:16.792517-07');
INSERT INTO "public"."django_migrations" VALUES ('17', 'Services', '0002_auto_20170420_1457', '2017-04-20 04:08:11.413792-07');
INSERT INTO "public"."django_migrations" VALUES ('18', 'Services', '0003_auto_20170420_1621', '2017-04-20 05:21:47.641386-07');
INSERT INTO "public"."django_migrations" VALUES ('19', 'Services', '0004_auto_20170420_1631', '2017-04-20 05:31:46.935663-07');
INSERT INTO "public"."django_migrations" VALUES ('20', 'core', '0001_initial', '2017-05-01 05:33:45.232228-07');
INSERT INTO "public"."django_migrations" VALUES ('21', 'core', '0002_auto_20170501_1654', '2017-05-01 05:54:23.171975-07');
INSERT INTO "public"."django_migrations" VALUES ('22', 'Services', '0001_changed_my_model', '2017-05-03 07:48:01.515652-07');
INSERT INTO "public"."django_migrations" VALUES ('23', 'Services', '0002_auto_20170503_1917', '2017-05-03 08:18:05.034807-07');
INSERT INTO "public"."django_migrations" VALUES ('24', 'core', '0002_portfolio', '2017-05-03 10:34:34.670034-07');
INSERT INTO "public"."django_migrations" VALUES ('25', 'core', '0003_auto_20170503_2153', '2017-05-03 10:53:15.578147-07');
INSERT INTO "public"."django_migrations" VALUES ('26', 'core', '0004_auto_20170504_1617', '2017-05-04 05:17:42.810546-07');
INSERT INTO "public"."django_migrations" VALUES ('27', 'core', '0002_contactus', '2017-05-04 07:59:02.14083-07');
INSERT INTO "public"."django_migrations" VALUES ('28', 'core', '0003_auto_20170505_1255', '2017-05-05 01:57:00.10758-07');
INSERT INTO "public"."django_migrations" VALUES ('29', 'core', '0004_auto_20170505_1258', '2017-05-05 01:59:08.411438-07');
INSERT INTO "public"."django_migrations" VALUES ('30', 'Services', '0003_auto_20170505_1508', '2017-05-05 04:09:06.192089-07');
INSERT INTO "public"."django_migrations" VALUES ('31', 'Services', '0004_auto_20170506_1114', '2017-05-06 00:14:19.840423-07');
INSERT INTO "public"."django_migrations" VALUES ('32', 'Auth', '0001_initial', '2017-05-06 01:34:25.793586-07');
INSERT INTO "public"."django_migrations" VALUES ('33', 'Auth', '0002_auto_20170508_1824', '2017-05-08 07:24:19.833998-07');
INSERT INTO "public"."django_migrations" VALUES ('34', 'Auth', '0003_auto_20170508_1827', '2017-05-08 07:27:41.760548-07');
INSERT INTO "public"."django_migrations" VALUES ('35', 'Auth', '0004_auto_20170509_1620', '2017-05-09 05:21:08.869349-07');
INSERT INTO "public"."django_migrations" VALUES ('36', 'Auth', '0005_auto_20170509_1640', '2017-05-09 05:41:24.916254-07');
INSERT INTO "public"."django_migrations" VALUES ('37', 'Auth', '0006_order_name', '2017-05-09 06:15:32.583828-07');
INSERT INTO "public"."django_migrations" VALUES ('38', 'Auth', '0007_order_orderlevel', '2017-05-09 07:03:04.060231-07');
INSERT INTO "public"."django_migrations" VALUES ('39', 'Auth', '0008_additionalservicelist_projectrecommendation_salerecommendation', '2017-05-09 07:20:15.989254-07');
INSERT INTO "public"."django_migrations" VALUES ('40', 'Auth', '0009_auto_20170510_0446', '2017-05-09 17:46:51.262427-07');

-- ----------------------------
-- Table structure for django_session
-- ----------------------------
DROP TABLE IF EXISTS "public"."django_session";
CREATE TABLE "public"."django_session" (
"session_key" varchar(40) COLLATE "default" NOT NULL,
"session_data" text COLLATE "default" NOT NULL,
"expire_date" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of django_session
-- ----------------------------
INSERT INTO "public"."django_session" VALUES ('2bb0xe6nakdwfai6j0puqiik8vg99xkz', 'MDkwYjEyOWQ1Y2Q3MjRjNDBjYzQyZDNmNzUyNWFkZGRlMjc3ZGNlNjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkY2MwZTE4ZDFiNmVhNjY4Yzg4ZDBkNTYxODFkYjg0M2JiN2VkNjk5IiwidXNlciI6Mjl9', '2017-05-25 03:39:58.496883-07');
INSERT INTO "public"."django_session" VALUES ('5jk4wqihk5a1b7p72ralcwn7miw07bje', 'NGU2YWY0NDQyM2NhZTBkZGVjNGMxNjE3YmU5NDk0ZjA1MTAzYzllZDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkY2MwZTE4ZDFiNmVhNjY4Yzg4ZDBkNTYxODFkYjg0M2JiN2VkNjk5In0=', '2017-04-28 04:04:04.174592-07');
INSERT INTO "public"."django_session" VALUES ('i5hyrq9fz5w1uuizzn3uhqxfg5q7ks1n', 'MTIzN2Q0NDdhOTMxNzM5YjMyZDRhZDYxZGVlM2Y2YWFmYzMxOTNlNzp7InVzZXIiOjI0fQ==', '2017-05-24 00:23:07.958831-07');

-- ----------------------------
-- Table structure for Services_companycategory
-- ----------------------------
DROP TABLE IF EXISTS "public"."Services_companycategory";
CREATE TABLE "public"."Services_companycategory" (
"id" int4 DEFAULT nextval('"Services_companycategory_id_seq"'::regclass) NOT NULL,
"name" varchar(200) COLLATE "default" NOT NULL,
"number" int4 NOT NULL,
"pub_date" timestamptz(6) NOT NULL,
"image" varchar(100) COLLATE "default" NOT NULL,
"slug" varchar(50) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of Services_companycategory
-- ----------------------------
INSERT INTO "public"."Services_companycategory" VALUES ('1', 'Project Firms', '0', '2017-05-03 07:51:09-07', 'uploads/company/category/bitcoinpaymantwallet.jpg', '0');
INSERT INTO "public"."Services_companycategory" VALUES ('2', 'Toruism Firms', '1', '2017-05-03 07:51:33-07', 'uploads/company/category/adobechatIndex.png', '0');

-- ----------------------------
-- Table structure for Services_companytype
-- ----------------------------
DROP TABLE IF EXISTS "public"."Services_companytype";
CREATE TABLE "public"."Services_companytype" (
"id" int4 DEFAULT nextval('"Services_companytype_id_seq"'::regclass) NOT NULL,
"name" varchar(200) COLLATE "default" NOT NULL,
"description" text COLLATE "default" NOT NULL,
"CompanyCategory_id" int4 NOT NULL,
"presentation" varchar(100) COLLATE "default" NOT NULL,
"slug" varchar(50) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of Services_companytype
-- ----------------------------
INSERT INTO "public"."Services_companytype" VALUES ('1', 'Construction Project Firms', 'Construction Project Firms provides the users number of project solutions', '1', 'uploads/company/type/presentation/Wildlife.mp4', 'construction-project-firms');
INSERT INTO "public"."Services_companytype" VALUES ('2', 'Business Project Firms', 'Business Project Firms provides to their customers business projecting', '1', 'uploads/company/type/presentation/Wildlife_YwBOjGo.mp4', 'bussiness-projects-firms');
INSERT INTO "public"."Services_companytype" VALUES ('3', 'Hotels', '<p>Hotel provides to their customers places to live temporary</p>

<h2>Am I eligible?</h2>

<p>You are eligible to apply if you are a High Potential Start-Up, this is defined as a company which is:</p>

<ul>
	<li>Based on an innovative technology or service offering;</li>
	<li>Likely to achieve significant growth in three to four years (sales of &euro;1m per annum and employment of 10 or more);</li>
	<li>Export orientated;</li>
	<li>Led by an experienced team, with a mixture of technical and commercial competencies.</li>
</ul>

<p>If you are interested in having an initial discussion, without commitment and with absolute confidentiality,&nbsp;and you&nbsp;believe your business idea meets Enterprise Ireland criteria, please go to our&nbsp;<a href="https://www.enterprise-ireland.com/en/Start-a-Business-in-Ireland/Do-I-qualify-as-a-HPSU-/Overview.html">Start a Business Section</a>&nbsp;to learn more about what we look for in a proposition and the contact details for the Start-up Team in your region.</p>

<p>&nbsp;</p>

<h2>What is the maximum HPSU feasibility grant and what costs are eligible?</h2>

<p>The maximum grant funding available for a High Potential Start-Up feasibility study is 50% of eligible expenditures.&nbsp; The maximum level of grant funding currently available is &euro;15,000.&nbsp;</p>

<ul>
	<li>For example, if your feasibility study costs &euro;30,000, Enterprise Ireland can grant aid a maximum of &euro;15,000.&nbsp; If your feasibility study expenditure is &euro;10,000, the Enterprise Ireland grant would be a maximum of &euro;5,000.&nbsp;&nbsp;</li>
</ul>

<p>Applications are considered on a case-by-case basis and the level of funding will be determined following assessment of;</p>

<p>(a) the merits of providing grant support to the activity set out in the application.<br />
(b) the need for financial support<br />
(c) previous funding provided to the company.<br />
(d) potential for employment and sales growth</p>

<p>Receipt of other grants from Enterprise Ireland may impact on your eligibility for support under this initiative.&nbsp;</p>', '2', 'uploads/company/type/presentation/Wildlife_PSBbRBe.mp4', 'hotels');

-- ----------------------------
-- Table structure for Services_service
-- ----------------------------
DROP TABLE IF EXISTS "public"."Services_service";
CREATE TABLE "public"."Services_service" (
"id" int4 DEFAULT nextval('"Services_service_id_seq"'::regclass) NOT NULL,
"name" varchar(200) COLLATE "default" NOT NULL,
"description" text COLLATE "default" NOT NULL,
"price" int4 NOT NULL,
"Support_id" int4 NOT NULL,
"slug" varchar(50) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of Services_service
-- ----------------------------
INSERT INTO "public"."Services_service" VALUES ('1', 'Human Resources', 'Human resource management (HRM or HR) is the management of human resources. It is designed to maximize employee performance in service of an employer''s strategic objectives.Human resource management (HRM or HR) is the management of human resources. It is designed to maximize employee performance in service of an employer''s strategic objectives.', '10', '3', 'human-resources');
INSERT INTO "public"."Services_service" VALUES ('2', 'Personnel Educating', 'Education Personnel is the leader in the education recruitment and supply market in the UK. Through our two specialist branded businesses, Teaching Personnel and Protocol Education, we operate over 60 branches, and provide classroom teachers, teaching and non-teaching support staff, nursery nurses and instructors for schools and other educational institutions..', '20', '3', 'personnel-education');
INSERT INTO "public"."Services_service" VALUES ('3', 'Salary Management', 'A General / Operations Manager earns an average salary of $60,296 per year. Experience has a moderate effect on income for this job. The highest paying skills associated with this job are Budget Management, Project Management, and Sales Management.A General / Operations Manager earns an average salary of $60,296 per year. Experience has a moderate effect on income for this job. The highest paying skills associated with this job are Budget Management, Project Management, and Sales Management.', '10', '3', 'salary-management');
INSERT INTO "public"."Services_service" VALUES ('4', 'Quality Verification', 'Verification and validation are independent procedures that are used together for checking that a product, service, or system meets requirements and specifications and that it fulfills its intended purpose. These are critical components of a quality management system such as ISOVerification and validation are independent procedures that are used together for checking that a product, service, or system meets requirements and specifications and that it fulfills its intended purpose. These are critical components of a quality management system such as ISOVerification and validation are independent procedures that are used together for checking that a product, service, or system meets requirements and specifications and that it fulfills its intended purpose. These are critical components of a quality management system such', '20', '2', 'quality-verification');
INSERT INTO "public"."Services_service" VALUES ('5', 'Serving Management', 'Servant leadership is both a leadership philosophy and set of leadership practices. Traditional ... to be first must be servant of all. 45 For even the Son of Man did not come to be served, but to serve, and to give his life as a ransom for many. .... Managers who empower and respect their staff get better performance in return.Servant leadership is both a leadership philosophy and set of leadership practices. Traditional ... to be first must be servant of all. 45 For even the Son of Man did not come to be served, but to serve, and to give his life as a ransom for many. .... Managers who empower and respect their staff get better performance in return.Servant leadership is both a leadership philosophy and set of leadership practices. Traditional ... to be first must be servant of all. 45 For even the Son of Man did not come to be served, but to serve, and to give his.', '20', '2', 'serving-management');
INSERT INTO "public"."Services_service" VALUES ('6', 'Outlay Decreasing', 'The Finance Division on Tuesday issued a revised budget authority which cut down the FY2015-16 budget outlay by 10.35%.

The revised budget outlay would be Tk264,565 crore, down by Tk30,535 crore from the original Tk295,100 crore budget outlay.

Senior Finance Secretary Mahbub Ahmed said that Finance Minister AMA Muhith would place the revised budget outlay to the current parliament session on May 2.

The budget outlay has been shrunk because the previous development funds could not be utilized.

One of the major issues for downsizing was the reduction of revenue collection by Tk26,370 crore from the actual outlay of Tk176,370 crore.

Mahbub said the shortage of revenue collection by the National Board of Revenue would also have a major impact on the total budget for the next fiscal year.The Finance Division on Tuesday issued a revised budget authority which cut down the FY2015-16 budget outlay by 10.35%.

The revised budget outlay would be Tk264,565 crore, down by Tk30,535 crore from the original Tk295,100 crore budget outlay.

Senior Finance Secretary Mahbub Ahmed said that Finance Minister AMA Muhith would place the revised budget outlay to the current parliament session on May 2.

The budget outlay has been shrunk because the previous development funds could not be utilized.

One of the major issues for downsizing was the reduction of revenue collection by Tk26,370 crore from the actual outlay of Tk176,370 crore.

Mahbub said the shortage of revenue collection by the National Board of Revenue would also have a major impact on the total budget for the next fiscal year.', '100', '2', 'outlay-decreasing');
INSERT INTO "public"."Services_service" VALUES ('7', 'Logistic Equipment Management', 'The related policies and procedures govern activities such as the selection, planning, and acquisition of medical devices, and the inspection, acceptance, maintenance, and eventual retirement and disposal of medical equipment. Medical equipment management is a recognized profession within the medical logistics domain.

Govern activities such as the selection, planning, and acquisition of medical devices, and the inspection, acceptance, maintenance, and eventual retirement and disposal of medical equipment. Medical equipment management is a recognized profession within the medical logistics domain.

The related policies and procedures govern activities such as the selection, planning, and acquisition of medical devices, and the inspection, acceptance, maintenance, and eventual retirement and disposal of medical equipment. Medical equipment management is a recognized profession within the medical logistics.', '20', '1', 'logistic-equipment-management');
INSERT INTO "public"."Services_service" VALUES ('8', 'Logistic Processes Management', 'Logistics management is the part of supply chain management that plans, implements, and controls the efficient, effective forward, and reverse flow and storage of goods, services, and related information between the point of origin and the point of consumption in order to meet customer''s requirements.

Management is the part of supply chain management that plans, implements, and controls the efficient, effective forward, and reverse flow and storage of goods, services, and related information between the point of origin and the point of consumption in order to meet customer''s requirements.
Logistics management is the part of supply chain management that plans, implements, and controls the efficient, effective forward, and reverse flow and storage of goods, services, and related information between the point of origin and the point of consumption in order to meet customer''s requirements.', '20', '1', 'logistic-processes-management');

-- ----------------------------
-- Table structure for Services_support
-- ----------------------------
DROP TABLE IF EXISTS "public"."Services_support";
CREATE TABLE "public"."Services_support" (
"id" int4 DEFAULT nextval('"Services_support_id_seq"'::regclass) NOT NULL,
"name" varchar(200) COLLATE "default" NOT NULL,
"description" text COLLATE "default" NOT NULL,
"presentation" varchar(100) COLLATE "default" NOT NULL,
"price" int4 NOT NULL,
"bonuses" text COLLATE "default" NOT NULL,
"realizationDuration" varchar(2000) COLLATE "default" NOT NULL,
"CompanyType_id" int4 NOT NULL,
"image" varchar(100) COLLATE "default" NOT NULL,
"slug" varchar(50) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of Services_support
-- ----------------------------
INSERT INTO "public"."Services_support" VALUES ('1', 'Logistic Support', 'Logistic Support Logistic Support Logistic Support Logistic Support Logistic Support Logistic Support Logistic Support Logistic Support Logistic Support Logistic Support Logistic Support Logistic Support Logistic Support Logistic Support Logistic Support Logistic Support Logistic Support Logistic Support Logistic Support Logistic Support Logistic Support Logistic', 'uploads/support/presentation/Wildlife.mp4', '3213', 'adasdasasdsa', 'two months', '3', 'uploads/support/image/newslittle.jpg', 'logistic-support');
INSERT INTO "public"."Services_support" VALUES ('2', 'Food & Catering Management', 'Food & Catering ManagementFood & Catering ManagementFood & Catering ManagementFood & Catering ManagementFood & Catering ManagementFood & Catering ManagementFood & Catering ManagementFood & Catering ManagementFood & Catering ManagementFood & Catering ManagementFood & Catering ManagementFood & Catering ManagementFood & Catering ManagementFood & Catering ManagementFood & Catering Management', 'uploads/support/presentation/Wildlife_ODbQGsJ.mp4', '500', 'Some Bonuses', '1 month', '3', 'uploads/support/image/image.jpg', 'food-catering-management');
INSERT INTO "public"."Services_support" VALUES ('3', 'Personnel Management', 'Cadro Management Cadro Management Cadro Management Cadro Management Cadro Management Cadro Management Cadro Management Cadro Management Cadro Management Cadro Management Cadro Management Cadro Management Cadro Management Cadro Management Cadro Management Cadro Management Cadro Management Cadro Management Cadro Management Cadro Management Cadro Management Cadro Management Cadro', 'uploads/support/presentation/Wildlife_A1pXzU1.mp4', '12', 'Some Bonuses', '2 month', '3', 'uploads/support/image/news1.jpg', 'personnel-management');
INSERT INTO "public"."Services_support" VALUES ('4', 'Accounting Management', 'Accounting Management Accounting Management Accounting Management Accounting Management Accounting Management Accounting Management Accounting Management Accounting Management Accounting Management Accounting Management Accounting Management Accounting Management Accounting Management Accounting', 'uploads/support/presentation/Wildlife_yfkIpXe.mp4', '2332', 'Some Bonuses', '3 months', '2', 'uploads/support/image/news.jpg', 'accouting-management');
INSERT INTO "public"."Services_support" VALUES ('5', 'Audit', 'Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of Project Firms Auditing of', 'uploads/support/presentation/Wildlife_7X9mZkp.mp4', '565', 'Some bonuses', '2 months', '2', 'uploads/support/image/newslittle1.jpg', 'aduit');
INSERT INTO "public"."Services_support" VALUES ('6', 'Investment Market Analyzing', 'Investment Market Analyzing Investment Market Analyzing Investment Market Analyzing Investment Market Analyzing Investment Market Analyzing Investment Market Analyzing Investment Market Analyzing Investment Market Analyzing Investment Market Analyzing Investment Market Analyzing Investment Market Analyzing Investment Market Analyzing Investment Market Analyzing Investment Market Analyzing Investment Market Analyzing Investment', 'uploads/support/presentation/Wildlife_N2JkHPT.mp4', '5000', 'Some Bonuses', '3 months', '1', 'uploads/support/image/goglePlanetsWeb.jpg', 'investmen-market-analyzing');
INSERT INTO "public"."Services_support" VALUES ('7', 'Investment Involving', 'Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment Involving Investment', 'uploads/support/presentation/Wildlife_nGFdlZ2.mp4', '2000', 'Some Bonuses', '2 months', '1', 'uploads/support/image/image_ovDapnP.jpg', 'investment-involving');

-- ----------------------------
-- Table structure for Services_supportadvantage
-- ----------------------------
DROP TABLE IF EXISTS "public"."Services_supportadvantage";
CREATE TABLE "public"."Services_supportadvantage" (
"id" int4 DEFAULT nextval('"Services_supportadvantage_id_seq"'::regclass) NOT NULL,
"name" varchar(200) COLLATE "default" NOT NULL,
"description" text COLLATE "default" NOT NULL,
"image" varchar(100) COLLATE "default" NOT NULL,
"lang" varchar(200) COLLATE "default" NOT NULL,
"Support_id" int4 NOT NULL,
"slug" varchar(50) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of Services_supportadvantage
-- ----------------------------
INSERT INTO "public"."Services_supportadvantage" VALUES ('1', 'Less Personnel', 'One of the main challenges facing tomorrow''s armed forces is improving their performance with fewer personnel available. TNO focuses on the deployability, sustainability, motivation, resilience and health of military personnel to maximize their human effectiveness in interaction with other humans and complex systems in demanding and complex environments.

Our main focus areas are Human Performance, Human Factors Integration and Training & Education within the broad fields of Human Factors and CBRN. Because cost benefits can be achieved through lower dropout rates and smaller units, TNO contributes by improving the training, monitoring and support of personnel, and by investigating the deployment of intelligent and (semi)autonomous systems.', 'chartBlock.png', 'en', '3', 'less-personnel');
INSERT INTO "public"."Services_supportadvantage" VALUES ('2', 'Increase effectivness', 'Our strength is the ability to combine all available expertise ranging from psychology (cognitive, social, organizational) to toxicology, within the research groups Human Behaviour & Organizational Innovations, Perceptual & Cognitive Systems, CBRN Protection and Training & Performance Innovations (see related Items). It is supported by a strong foundation of military domain knowledge.
Our strength is the ability to combine all available expertise ranging from psychology (cognitive, social, organizational) to toxicology, within the research groups Human Behaviour & Organizational Innovations, Perceptual & Cognitive Systems, CBRN Protection and Training & Performance Innovations (see related Items). It is supported by a strong foundation of military domain knowledge.', 'chartLine.png', 'en', '3', 'increas-effectiveness');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
ALTER SEQUENCE "public"."Auth_additionalservicelist_id_seq" OWNED BY "Auth_additionalservicelist"."id";
ALTER SEQUENCE "public"."auth_group_id_seq" OWNED BY "auth_group"."id";
ALTER SEQUENCE "public"."auth_group_permissions_id_seq" OWNED BY "auth_group_permissions"."id";
ALTER SEQUENCE "public"."Auth_order_id_seq" OWNED BY "Auth_order"."id";
ALTER SEQUENCE "public"."auth_permission_id_seq" OWNED BY "auth_permission"."id";
ALTER SEQUENCE "public"."Auth_projectrecommendation_id_seq" OWNED BY "Auth_projectrecommendation"."id";
ALTER SEQUENCE "public"."auth_user_groups_id_seq" OWNED BY "auth_user_groups"."id";
ALTER SEQUENCE "public"."auth_user_id_seq" OWNED BY "auth_user"."id";
ALTER SEQUENCE "public"."Auth_user_id_seq" OWNED BY "Auth_user"."id";
ALTER SEQUENCE "public"."auth_user_user_permissions_id_seq" OWNED BY "auth_user_user_permissions"."id";
ALTER SEQUENCE "public"."core_bsssimagination_id_seq" OWNED BY "core_bsssimagination"."id";
ALTER SEQUENCE "public"."core_contactus_id_seq" OWNED BY "core_contactus"."id";
ALTER SEQUENCE "public"."core_howdoesitwork_id_seq" OWNED BY "core_howdoesitwork"."id";
ALTER SEQUENCE "public"."core_menu_id_seq" OWNED BY "core_menu"."id";
ALTER SEQUENCE "public"."core_partners_id_seq" OWNED BY "core_partner"."id";
ALTER SEQUENCE "public"."core_portfolio_id_seq" OWNED BY "core_portfolio"."id";
ALTER SEQUENCE "public"."core_portfolioimages_id_seq" OWNED BY "core_portfolioimages"."id";
ALTER SEQUENCE "public"."core_slide_id_seq" OWNED BY "core_slide"."id";
ALTER SEQUENCE "public"."core_wwusadvantage_id_seq" OWNED BY "core_wwusadvantage"."id";
ALTER SEQUENCE "public"."django_admin_log_id_seq" OWNED BY "django_admin_log"."id";
ALTER SEQUENCE "public"."django_content_type_id_seq" OWNED BY "django_content_type"."id";
ALTER SEQUENCE "public"."django_migrations_id_seq" OWNED BY "django_migrations"."id";
ALTER SEQUENCE "public"."Services_companycategory_id_seq" OWNED BY "Services_companycategory"."id";
ALTER SEQUENCE "public"."Services_companytype_id_seq" OWNED BY "Services_companytype"."id";
ALTER SEQUENCE "public"."Services_service_id_seq" OWNED BY "Services_service"."id";
ALTER SEQUENCE "public"."Services_support_id_seq" OWNED BY "Services_support"."id";
ALTER SEQUENCE "public"."Services_supportadvantage_id_seq" OWNED BY "Services_supportadvantage"."id";

-- ----------------------------
-- Indexes structure for table Auth_additionalservicelist
-- ----------------------------
CREATE INDEX "Auth_additionalservicelist_Order_id_4365b11e" ON "public"."Auth_additionalservicelist" USING btree ("Order_id");
CREATE INDEX "Auth_additionalservicelist_Service_id_61f07dfd" ON "public"."Auth_additionalservicelist" USING btree ("Service_id");

-- ----------------------------
-- Primary Key structure for table Auth_additionalservicelist
-- ----------------------------
ALTER TABLE "public"."Auth_additionalservicelist" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table auth_group
-- ----------------------------
CREATE INDEX "auth_group_name_a6ea08ec_like" ON "public"."auth_group" USING btree ("name" "pg_catalog"."varchar_pattern_ops");

-- ----------------------------
-- Uniques structure for table auth_group
-- ----------------------------
ALTER TABLE "public"."auth_group" ADD UNIQUE ("name");

-- ----------------------------
-- Primary Key structure for table auth_group
-- ----------------------------
ALTER TABLE "public"."auth_group" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table auth_group_permissions
-- ----------------------------
CREATE INDEX "auth_group_permissions_group_id_b120cbf9" ON "public"."auth_group_permissions" USING btree ("group_id");
CREATE INDEX "auth_group_permissions_permission_id_84c5c92e" ON "public"."auth_group_permissions" USING btree ("permission_id");

-- ----------------------------
-- Uniques structure for table auth_group_permissions
-- ----------------------------
ALTER TABLE "public"."auth_group_permissions" ADD UNIQUE ("group_id", "permission_id");

-- ----------------------------
-- Primary Key structure for table auth_group_permissions
-- ----------------------------
ALTER TABLE "public"."auth_group_permissions" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table Auth_order
-- ----------------------------
CREATE INDEX "Auth_order_OrderSupport_id_ee631e02" ON "public"."Auth_order" USING btree ("Support_id");
CREATE INDEX "Auth_order_OrderUser_id_060696ff" ON "public"."Auth_order" USING btree ("User_id");

-- ----------------------------
-- Primary Key structure for table Auth_order
-- ----------------------------
ALTER TABLE "public"."Auth_order" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table auth_permission
-- ----------------------------
CREATE INDEX "auth_permission_content_type_id_2f476e4b" ON "public"."auth_permission" USING btree ("content_type_id");

-- ----------------------------
-- Uniques structure for table auth_permission
-- ----------------------------
ALTER TABLE "public"."auth_permission" ADD UNIQUE ("content_type_id", "codename");

-- ----------------------------
-- Primary Key structure for table auth_permission
-- ----------------------------
ALTER TABLE "public"."auth_permission" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table Auth_projectrecommendation
-- ----------------------------
CREATE INDEX "Auth_projectrecommendation_Order_id_5b9bf676" ON "public"."Auth_projectrecommendation" USING btree ("Order_id");
CREATE INDEX "Auth_projectrecommendation_Service_id_ee38ccec" ON "public"."Auth_projectrecommendation" USING btree ("Service_id");
CREATE INDEX "Auth_projectrecommendation_User_id_8f6ad0f1" ON "public"."Auth_projectrecommendation" USING btree ("User_id");

-- ----------------------------
-- Primary Key structure for table Auth_projectrecommendation
-- ----------------------------
ALTER TABLE "public"."Auth_projectrecommendation" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table auth_user
-- ----------------------------
CREATE INDEX "auth_user_username_6821ab7c_like" ON "public"."auth_user" USING btree ("username" "pg_catalog"."varchar_pattern_ops");

-- ----------------------------
-- Uniques structure for table auth_user
-- ----------------------------
ALTER TABLE "public"."auth_user" ADD UNIQUE ("username");

-- ----------------------------
-- Primary Key structure for table auth_user
-- ----------------------------
ALTER TABLE "public"."auth_user" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table Auth_user
-- ----------------------------
ALTER TABLE "public"."Auth_user" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table auth_user_groups
-- ----------------------------
CREATE INDEX "auth_user_groups_user_id_6a12ed8b" ON "public"."auth_user_groups" USING btree ("user_id");
CREATE INDEX "auth_user_groups_group_id_97559544" ON "public"."auth_user_groups" USING btree ("group_id");

-- ----------------------------
-- Uniques structure for table auth_user_groups
-- ----------------------------
ALTER TABLE "public"."auth_user_groups" ADD UNIQUE ("user_id", "group_id");

-- ----------------------------
-- Primary Key structure for table auth_user_groups
-- ----------------------------
ALTER TABLE "public"."auth_user_groups" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table auth_user_user_permissions
-- ----------------------------
CREATE INDEX "auth_user_user_permissions_user_id_a95ead1b" ON "public"."auth_user_user_permissions" USING btree ("user_id");
CREATE INDEX "auth_user_user_permissions_permission_id_1fbb5f2c" ON "public"."auth_user_user_permissions" USING btree ("permission_id");

-- ----------------------------
-- Uniques structure for table auth_user_user_permissions
-- ----------------------------
ALTER TABLE "public"."auth_user_user_permissions" ADD UNIQUE ("user_id", "permission_id");

-- ----------------------------
-- Primary Key structure for table auth_user_user_permissions
-- ----------------------------
ALTER TABLE "public"."auth_user_user_permissions" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table core_bsssimagination
-- ----------------------------
ALTER TABLE "public"."core_bsssimagination" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table core_contactus
-- ----------------------------
ALTER TABLE "public"."core_contactus" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table core_howdoesitwork
-- ----------------------------
ALTER TABLE "public"."core_howdoesitwork" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table core_menu
-- ----------------------------
ALTER TABLE "public"."core_menu" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table core_partner
-- ----------------------------
ALTER TABLE "public"."core_partner" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table core_portfolio
-- ----------------------------
ALTER TABLE "public"."core_portfolio" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table core_portfolioimages
-- ----------------------------
CREATE INDEX "core_portfolioimages_portfolio_id_0523442d" ON "public"."core_portfolioimages" USING btree ("portfolio_id");

-- ----------------------------
-- Primary Key structure for table core_portfolioimages
-- ----------------------------
ALTER TABLE "public"."core_portfolioimages" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table core_slide
-- ----------------------------
ALTER TABLE "public"."core_slide" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table core_wwusadvantage
-- ----------------------------
ALTER TABLE "public"."core_wwusadvantage" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table django_admin_log
-- ----------------------------
CREATE INDEX "django_admin_log_content_type_id_c4bce8eb" ON "public"."django_admin_log" USING btree ("content_type_id");
CREATE INDEX "django_admin_log_user_id_c564eba6" ON "public"."django_admin_log" USING btree ("user_id");

-- ----------------------------
-- Checks structure for table django_admin_log
-- ----------------------------
ALTER TABLE "public"."django_admin_log" ADD CHECK ((action_flag >= 0));

-- ----------------------------
-- Primary Key structure for table django_admin_log
-- ----------------------------
ALTER TABLE "public"."django_admin_log" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table django_content_type
-- ----------------------------
ALTER TABLE "public"."django_content_type" ADD UNIQUE ("app_label", "model");

-- ----------------------------
-- Primary Key structure for table django_content_type
-- ----------------------------
ALTER TABLE "public"."django_content_type" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table django_migrations
-- ----------------------------
ALTER TABLE "public"."django_migrations" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table django_session
-- ----------------------------
CREATE INDEX "django_session_session_key_c0390e0f_like" ON "public"."django_session" USING btree ("session_key" "pg_catalog"."varchar_pattern_ops");
CREATE INDEX "django_session_expire_date_a5c62663" ON "public"."django_session" USING btree ("expire_date");

-- ----------------------------
-- Primary Key structure for table django_session
-- ----------------------------
ALTER TABLE "public"."django_session" ADD PRIMARY KEY ("session_key");

-- ----------------------------
-- Indexes structure for table Services_companycategory
-- ----------------------------
CREATE INDEX "Services_companycategory_slug_9908f610" ON "public"."Services_companycategory" USING btree ("slug");
CREATE INDEX "Services_companycategory_slug_9908f610_like" ON "public"."Services_companycategory" USING btree ("slug" "pg_catalog"."varchar_pattern_ops");

-- ----------------------------
-- Primary Key structure for table Services_companycategory
-- ----------------------------
ALTER TABLE "public"."Services_companycategory" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table Services_companytype
-- ----------------------------
CREATE INDEX "Services_companytype_CompanyCategory_id_4743dc1b" ON "public"."Services_companytype" USING btree ("CompanyCategory_id");
CREATE INDEX "Services_companytype_slug_59b0bc42" ON "public"."Services_companytype" USING btree ("slug");
CREATE INDEX "Services_companytype_slug_59b0bc42_like" ON "public"."Services_companytype" USING btree ("slug" "pg_catalog"."varchar_pattern_ops");

-- ----------------------------
-- Primary Key structure for table Services_companytype
-- ----------------------------
ALTER TABLE "public"."Services_companytype" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table Services_service
-- ----------------------------
CREATE INDEX "Services_service_Support_id_2d8bfd51" ON "public"."Services_service" USING btree ("Support_id");
CREATE INDEX "Services_service_slug_050f2418" ON "public"."Services_service" USING btree ("slug");
CREATE INDEX "Services_service_slug_050f2418_like" ON "public"."Services_service" USING btree ("slug" "pg_catalog"."varchar_pattern_ops");

-- ----------------------------
-- Primary Key structure for table Services_service
-- ----------------------------
ALTER TABLE "public"."Services_service" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table Services_support
-- ----------------------------
CREATE INDEX "Services_support_CompanyType_id_57e4755c" ON "public"."Services_support" USING btree ("CompanyType_id");
CREATE INDEX "Services_support_slug_dd72312f" ON "public"."Services_support" USING btree ("slug");
CREATE INDEX "Services_support_slug_dd72312f_like" ON "public"."Services_support" USING btree ("slug" "pg_catalog"."varchar_pattern_ops");

-- ----------------------------
-- Primary Key structure for table Services_support
-- ----------------------------
ALTER TABLE "public"."Services_support" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table Services_supportadvantage
-- ----------------------------
CREATE INDEX "Services_supportadvantage_Support_id_320478c7" ON "public"."Services_supportadvantage" USING btree ("Support_id");
CREATE INDEX "Services_supportadvantage_slug_d95bc596" ON "public"."Services_supportadvantage" USING btree ("slug");
CREATE INDEX "Services_supportadvantage_slug_d95bc596_like" ON "public"."Services_supportadvantage" USING btree ("slug" "pg_catalog"."varchar_pattern_ops");

-- ----------------------------
-- Primary Key structure for table Services_supportadvantage
-- ----------------------------
ALTER TABLE "public"."Services_supportadvantage" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Key structure for table "public"."Auth_additionalservicelist"
-- ----------------------------
ALTER TABLE "public"."Auth_additionalservicelist" ADD FOREIGN KEY ("Order_id") REFERENCES "public"."Auth_order" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."Auth_additionalservicelist" ADD FOREIGN KEY ("Service_id") REFERENCES "public"."Services_service" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Key structure for table "public"."auth_group_permissions"
-- ----------------------------
ALTER TABLE "public"."auth_group_permissions" ADD FOREIGN KEY ("permission_id") REFERENCES "public"."auth_permission" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."auth_group_permissions" ADD FOREIGN KEY ("group_id") REFERENCES "public"."auth_group" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Key structure for table "public"."Auth_order"
-- ----------------------------
ALTER TABLE "public"."Auth_order" ADD FOREIGN KEY ("Support_id") REFERENCES "public"."Services_support" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."Auth_order" ADD FOREIGN KEY ("User_id") REFERENCES "public"."Auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Key structure for table "public"."auth_permission"
-- ----------------------------
ALTER TABLE "public"."auth_permission" ADD FOREIGN KEY ("content_type_id") REFERENCES "public"."django_content_type" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Key structure for table "public"."Auth_projectrecommendation"
-- ----------------------------
ALTER TABLE "public"."Auth_projectrecommendation" ADD FOREIGN KEY ("Order_id") REFERENCES "public"."Auth_order" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."Auth_projectrecommendation" ADD FOREIGN KEY ("Service_id") REFERENCES "public"."Services_service" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."Auth_projectrecommendation" ADD FOREIGN KEY ("User_id") REFERENCES "public"."Auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Key structure for table "public"."auth_user_groups"
-- ----------------------------
ALTER TABLE "public"."auth_user_groups" ADD FOREIGN KEY ("group_id") REFERENCES "public"."auth_group" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."auth_user_groups" ADD FOREIGN KEY ("user_id") REFERENCES "public"."auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Key structure for table "public"."auth_user_user_permissions"
-- ----------------------------
ALTER TABLE "public"."auth_user_user_permissions" ADD FOREIGN KEY ("user_id") REFERENCES "public"."auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."auth_user_user_permissions" ADD FOREIGN KEY ("permission_id") REFERENCES "public"."auth_permission" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Key structure for table "public"."core_portfolioimages"
-- ----------------------------
ALTER TABLE "public"."core_portfolioimages" ADD FOREIGN KEY ("portfolio_id") REFERENCES "public"."core_portfolio" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Key structure for table "public"."django_admin_log"
-- ----------------------------
ALTER TABLE "public"."django_admin_log" ADD FOREIGN KEY ("content_type_id") REFERENCES "public"."django_content_type" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."django_admin_log" ADD FOREIGN KEY ("user_id") REFERENCES "public"."auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Key structure for table "public"."Services_companytype"
-- ----------------------------
ALTER TABLE "public"."Services_companytype" ADD FOREIGN KEY ("CompanyCategory_id") REFERENCES "public"."Services_companycategory" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Key structure for table "public"."Services_service"
-- ----------------------------
ALTER TABLE "public"."Services_service" ADD FOREIGN KEY ("Support_id") REFERENCES "public"."Services_support" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Key structure for table "public"."Services_support"
-- ----------------------------
ALTER TABLE "public"."Services_support" ADD FOREIGN KEY ("CompanyType_id") REFERENCES "public"."Services_companytype" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Key structure for table "public"."Services_supportadvantage"
-- ----------------------------
ALTER TABLE "public"."Services_supportadvantage" ADD FOREIGN KEY ("Support_id") REFERENCES "public"."Services_support" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
