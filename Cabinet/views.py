from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect


from django.template import loader

from django.shortcuts import get_object_or_404, render

from Services.models import Support

from Services.models import Service
from Services.models import SupportAdvantage

from Auth.models import Order

from Auth.models import ProjectRecommendation
from Auth.models import AdditionalServiceList



from .forms import AddToMyOrderForm
# Create your views here.
def index(request):
	if(request.session['user']):
		MyUser = request.session['user']

	OrderList = Order.objects.filter(User=MyUser)
	context = {
		'MyUser': MyUser,
		'OrderList': OrderList,
	}
	template = loader.get_template('cabinet/index.html')
	return HttpResponse(template.render(context, request))

def CabinetOrderPage(request,OrderSlug,OrderLevel):
	if(request.session['user']):
		MyUser = request.session['user']

	OrderList = Order.objects.filter(User=MyUser)

	MyOrder = get_object_or_404(Order,slug=OrderSlug,User=MyUser)
	MySupport = get_object_or_404(Support,id=MyOrder.Support_id)
	ServiceList = Service.objects.filter(Support_id=MySupport.id)
	SupportAdvantageList = SupportAdvantage.objects.filter(Support_id=MySupport.id)


	ProjectRecommendationList = 0
	GeneralServiceList = 0
	MyAdditionalServiceList = 0
	if OrderLevel != '1':
		false = 'f'
		ProjectRecommendationList = ProjectRecommendation.objects.filter(Order=MyOrder.id,Apply=false)
		GeneralServiceList = Service.objects.order_by('id')
		MyAdditionalServiceList = AdditionalServiceList.objects.filter(Order=MyOrder.id)
	# if OrderLevel == '3':
		
	
	context = {
		'OrderLevel': OrderLevel,
		'MyAdditionalServiceList': MyAdditionalServiceList,
		'OrderList': OrderList,
		'GeneralServiceList': GeneralServiceList,
		'ProjectRecommendationList': ProjectRecommendationList,
		'SupportAdvantageList':SupportAdvantageList,
		'MySupport': MySupport,
		'ServiceList': ServiceList,
	}
	template = loader.get_template('cabinet/supportpage.html')
	return HttpResponse(template.render(context,request))

def SubmitProcess(request,OrderSlug,OrderLevel):
	if(request.session['user']):
		MyUser = request.session['user']


	MyOrder = get_object_or_404(Order,slug=OrderSlug,User=MyUser)
	MySupport = get_object_or_404(Support,id=MyOrder.Support_id)
	ServiceList = Service.objects.filter(Support_id=MySupport.id)

	ProjectRecommendationList = 0
	GeneralServiceList = 0
	if OrderLevel == '2':
		ProjectRecommendationList = ProjectRecommendation.objects.filter(Order=MyOrder.id)
		GeneralServiceList = Service.objects.order_by('id')

	if(int(OrderLevel) < 3):
		OrderLevelIncrease = int(OrderLevel)+1
		MyOrder = get_object_or_404(Order,slug=OrderSlug,User=MyUser)
		MyOrder.OrderLevel = MyOrder.OrderLevel+1
		MyOrder.save()
	

	
	context = {
		'GeneralServiceList': GeneralServiceList,
		'ProjectRecommendationList': ProjectRecommendationList,
		'MySupport': MySupport,
		'ServiceList': ServiceList,
	}
	return redirect('http://127.0.0.1:8000/cabinet/order/'+OrderSlug+'/level-'+str(OrderLevelIncrease))

def AddToMyOrder(request,OrderSlug,OrderLevel):
	if(request.session['user']):
		MyUser = request.session['user']

	if request.method == 'POST':
		# create a form instance and populate it with data from the request:
		form = AddToMyOrderForm(request.POST)
		# check whether it's valid:
		if form.is_valid():
			MyOrder = get_object_or_404(Order,slug=OrderSlug,User=MyUser)

			selectedServices = form.cleaned_data['selectedServices']
			selectedServices.split(',')

			counter = 0
			lengthSS = len(selectedServices)
			while (counter < lengthSS):
				MyProjectRecommendation = get_object_or_404(ProjectRecommendation,Order=MyOrder,User=MyUser,Service_id=selectedServices[counter])
				MyProjectRecommendation.Apply = 't'
				MyProjectRecommendation.save()

				NewAdditionalService = AdditionalServiceList(Order=MyOrder,Service_id=selectedServices[counter])
				NewAdditionalService.save()
				counter = counter+2

			Response = 'Response'
			return redirect('http://127.0.0.1:8000/cabinet/order/'+OrderSlug+'/level-'+str(OrderLevel))

def ServicePage(request,ServiceSlug):
	if(request.session['user']):
		MyUser = request.session['user']

	MyService = get_object_or_404(Service,slug=ServiceSlug)

	# MyOrder = get_object_or_404(Order,slug=OrderSlug,User=MyUser)
	# MySupport = get_object_or_404(Support,id=MyOrder.Support_id)
	# ServiceList = Service.objects.filter(Support_id=MySupport.id)


	# if OrderLevel == '3':
		
	
	context = {
		# 'OrderLevel': OrderLevel,
		# 'MyAdditionalServiceList': MyAdditionalServiceList,
		# 'OrderList': OrderList,
		# 'GeneralServiceList': GeneralServiceList,
		# 'ProjectRecommendationList': ProjectRecommendationList,
		# 'MySupport': MySupport,
		'MyService': MyService,
	}
	template = loader.get_template('cabinet/servicepage.html')
	return HttpResponse(template.render(context,request))		