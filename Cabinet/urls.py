from django.conf.urls import url
from django.conf.urls import include

#import Auth
from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<ServiceSlug>[\w-]+)/$', views.ServicePage, name='ServicePage'),
    url(r'^order/(?P<OrderSlug>[\w-]+)/level-(?P<OrderLevel>[0-9]+)/$', views.CabinetOrderPage, name='CabinetOrderPage'),
    url(r'^order/(?P<OrderSlug>[\w-]+)/level-(?P<OrderLevel>[0-9]+)/submitprocess/$', views.SubmitProcess, name='SubmitProcess'),
    url(r'^order/(?P<OrderSlug>[\w-]+)/level-(?P<OrderLevel>[0-9]+)/addToMyOrder/$', views.AddToMyOrder, name='AddToMyOrder'),
    #url(r'^(?P<CompanyTypeSlug>[\w-]+)/(?P<SupportSlug>[\w-]+)/order/$', views.OrderPage, name='OrderPage'),

    #url(r'^(?P<Company_id>[0-9]+)/$', views.CompanyTypePage, name='CompanyTypePage'),
    # url(r'^(?P<Company_id>[0-9]+)/package-(?P<Package_id>[0-9]+)/$', views.PackageDetails, name='PackageDetails'),
]

